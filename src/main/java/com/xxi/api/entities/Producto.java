package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PRODUCTO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listProductos",
		procedureName="PACK_PRODUCTO.GET",
		resultClasses = Producto.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createProducto",
		procedureName="PACK_PRODUCTO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_stock", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_min", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_unidad", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateProducto",
	procedureName="PACK_PRODUCTO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_stock", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_min", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_unidad", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteProducto",
	procedureName="PACK_PRODUCTO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Producto {
	
	//Atributos
	@Column(name = "IDPROD")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idProducto;
	
	@Column(name = "NOMBREPROD", nullable = false, length = 50)
	private String nombreProducto;
	
	@Column(name = "STOCKPROD", nullable = false, precision=30, scale=2)
	private Double stockProducto;
	
	@Column(name = "STOCKMINPROD", nullable = false, precision=30, scale=2)
	private Double stockMinimo;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDUNIMED")
    private UnidadMedida unidad;
	
	//Constructores	
	protected Producto() {
	}

	public Producto(Integer idProducto, UnidadMedida unidad, String nombreProducto, Double stockProducto,
			Double stockMinimo) {
		super();
		this.idProducto = idProducto;
		this.unidad = unidad;
		this.nombreProducto = nombreProducto;
		this.stockProducto = stockProducto;
		this.stockMinimo = stockMinimo;
	}

	public Integer getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public UnidadMedida getUnidad() {
		return unidad;
	}

	public void setUnidad(UnidadMedida unidad) {
		this.unidad = unidad;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public Double getStockProducto() {
		return stockProducto;
	}

	public void setStockProducto(Double stockProducto) {
		this.stockProducto = stockProducto;
	}

	public Double getStockMinimo() {
		return stockMinimo;
	}

	public void setStockMinimo(Double stockMinimo) {
		this.stockMinimo = stockMinimo;
	}

	@Override
	public String toString() {
		return "Producto [idProducto=" + idProducto + ", unidad=" + unidad + ", nombreProducto=" + nombreProducto
				+ ", stockProducto=" + stockProducto + ", stockMinimo=" + stockMinimo + "]";
	}
	

	
}