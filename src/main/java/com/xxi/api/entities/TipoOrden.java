package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "TIPO_ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listTiposOrden",
			procedureName="PACK_TIPO_ORDEN.GET",
			resultClasses = TipoOrden.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createTipoOrden",
			procedureName="PACK_TIPO_ORDEN.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updateTipoOrden",
	procedureName="PACK_TIPO_ORDEN.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delTipoOrden",
	procedureName="PACK_TIPO_ORDEN.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class TipoOrden {

	@Column(name = "IDTIPOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idTipo;
	@Column(name = "NOMBRETIPOR", nullable = false, length = 50)
	private String nombreTipo;
	
	//Constructor
	public TipoOrden(Integer idTipo, String nombreTipo) {
		super();
		this.idTipo = idTipo;
		this.nombreTipo = nombreTipo;
	}
	
	protected TipoOrden() {
		
	}
	
	//Getters & Setters
	public Integer getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	
	//toString();
	@Override
	public String toString() {
		return "TipoOrden [idTipo=" + idTipo + ", nombreTipo=" + nombreTipo + "]";
	}
	
}