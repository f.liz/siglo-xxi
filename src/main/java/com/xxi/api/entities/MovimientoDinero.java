package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIMIENTO_DINERO") 
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listMovimientos",
			procedureName="PACK_MOVIMIENTO_DINERO.GET",
			resultClasses = MovimientoDinero.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createMovimiento",
			procedureName="PACK_MOVIMIENTO_DINERO.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_tipo", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_desc", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updateMovimiento",
	procedureName="PACK_MOVIMIENTO_DINERO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_tipo", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_desc", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delMovimiento",
	procedureName="PACK_MOVIMIENTO_DINERO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class MovimientoDinero {
	@Column(name = "IDMOV")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idMovimiento;
	@Column(name = "MONTOMOV", nullable = false, length = 30)
	private Integer montoMovimiento;
	@Column(name = "FECHAMOV", nullable = false)
	private Date fechaMovimiento;
	@Column(name = "TIPOMOV", nullable = false, length = 1)
	private String tipoMovimiento;
	@Column(name = "DESCRIPCIONMOV", nullable = true, length = 50)
	private String descripcionMovimiento;
	
	//Constructores	
	protected MovimientoDinero() {
	}
	
	public MovimientoDinero(Integer idMovimiento, Integer montoMovimiento, String tipoMovimiento,
			String descripcionMovimiento, Date fechaMovimiento) {
		super();
		this.idMovimiento = idMovimiento;
		this.montoMovimiento = montoMovimiento;
		this.tipoMovimiento = tipoMovimiento;
		this.descripcionMovimiento = descripcionMovimiento;
		this.fechaMovimiento = fechaMovimiento;
	}
	
	public Integer getIdMovimiento() {
		return idMovimiento;
	}
	public void setIdMovimiento(Integer idMovimiento) {
		this.idMovimiento = idMovimiento;
	}
	public Integer getMontoMovimiento() {
		return montoMovimiento;
	}
	public void setMontoMovimiento(Integer montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}
	public void setDescripcionMovimiento(String descripcionMovimiento) {
		this.descripcionMovimiento = descripcionMovimiento;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	
	@Override
	public String toString() {
		return "MovimientoDinero [idMovimiento=" + idMovimiento + ", montoMovimiento=" + montoMovimiento
				+ ", tipoMovimiento=" + tipoMovimiento + ", descripcionMovimiento=" + descripcionMovimiento
				+ ", fechaMovimiento=" + fechaMovimiento + "]";
	}
	
}
