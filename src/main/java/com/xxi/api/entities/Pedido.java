package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PEDIDO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPedidos",
		procedureName="PACK_PEDIDO.GET",
		resultClasses = Pedido.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createPedido",
		procedureName="PACK_PEDIDO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_total", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updatePedido",
	procedureName="PACK_PEDIDO.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_total", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fecha", type=Date.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deletePedido",
	procedureName="PACK_PEDIDO.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Pedido {
	
	//Atributos
	@Column(name = "IDPEDIDO")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPedido;
	
	@Column(name = "TOTALPEDIDO", nullable = false, length = 30)
	private Integer totalPedido;
	
	@Column(name = "FECHAPEDIDO", nullable = false)
	private Date fechaPedido;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDUSUARIO")
    private Usuario usuario;
	
	//Constructores	
	protected Pedido() {
	}

	public Pedido(Integer idPedido, Date fechaPedido, Integer totalPedido, Usuario usuario) {
		super();
		this.idPedido = idPedido;
		this.fechaPedido = fechaPedido;
		this.totalPedido = totalPedido;
		this.usuario = usuario;
	}

	public Integer getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Integer getTotalPedido() {
		return totalPedido;
	}

	public void setTotalPedido(Integer totalPedido) {
		this.totalPedido = totalPedido;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Pedido [idPedido=" + idPedido + ", fechaPedido=" + fechaPedido + ", totalPedido=" + totalPedido
				+ ", usuario=" + usuario + "]";
	}
	
}