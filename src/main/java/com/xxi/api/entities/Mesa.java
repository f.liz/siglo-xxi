package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "MESA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listMesas",
		procedureName="PACK_MESA.GET",
		resultClasses = Mesa.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createMesa",
		procedureName="PACK_MESA.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_numero", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_capacidad", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updateMesa",
	procedureName="PACK_MESA.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_numero", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_capacidad", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deleteMesa",
	procedureName="PACK_MESA.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Mesa {
	
	//Atributos
	@Column(name = "IDMESA")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idMesa;
	
	@Column(name = "NUMEROMESA", nullable = false, length = 30)
	private Integer numeroMesa;
	
	@Column(name = "CAPACIDADMESA", nullable = false, length = 30)
	private Integer capacidadMesa;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTMESA")
    private EstadoMesa estado;
	
	//Constructores	
	protected Mesa() {
	}

	public Mesa(Integer idMesa, Integer numeroMesa, Integer capacidadMesa, EstadoMesa estado) {
		super();
		this.idMesa = idMesa;
		this.numeroMesa = numeroMesa;
		this.capacidadMesa = capacidadMesa;
		this.estado = estado;
	}

	public Integer getIdMesa() {
		return idMesa;
	}

	public void setIdMesa(Integer idMesa) {
		this.idMesa = idMesa;
	}

	public Integer getNumeroMesa() {
		return numeroMesa;
	}

	public void setNumeroMesa(Integer numeroMesa) {
		this.numeroMesa = numeroMesa;
	}

	public Integer getCapacidadMesa() {
		return capacidadMesa;
	}

	public void setCapacidadMesa(Integer capacidadMesa) {
		this.capacidadMesa = capacidadMesa;
	}

	public EstadoMesa getEstado() {
		return estado;
	}

	public void setEstado(EstadoMesa estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Mesa [idMesa=" + idMesa + ", numeroMesa=" + numeroMesa + ", capacidadMesa=" + capacidadMesa
				+ ", estado=" + estado + "]";
	}
	
}