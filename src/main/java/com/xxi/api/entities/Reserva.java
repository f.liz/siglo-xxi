package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "RESERVA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listReservas",
		procedureName="PACK_RESERVA.GET",
		resultClasses = Reserva.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createReserva",
		procedureName="PACK_RESERVA.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechaini", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechater", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_rut", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmesa", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateReserva",
	procedureName="PACK_RESERVA.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idreserva", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechaini", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechater", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_rut", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmesa", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteReserva",
	procedureName="PACK_RESERVA.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idreserva", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Reserva {
	
	//Atributos
	@Column(name = "IDRES")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idReserva;
	
	@Column(name = "FECHAINIRES", nullable = false)
	private Date fechaInicio;
	
	@Column(name = "FECHATERRES", nullable = false)
	private Date fechaTermino;
	
	@Column(name = "RUTRES", nullable = false, length = 11)
	private String rutReserva;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMESA")
    private Mesa mesa;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDUSUARIO")
    private Usuario usuario;
	
	//Constructores	
	protected Reserva() {
	}

	public Reserva(Integer idReserva, Date fechaInicio, Date fechaTermino, String rutReserva, Mesa mesa,
			Usuario usuario) {
		super();
		this.idReserva = idReserva;
		this.fechaInicio = fechaInicio;
		this.fechaTermino = fechaTermino;
		this.rutReserva = rutReserva;
		this.mesa = mesa;
		this.usuario = usuario;
	}

	public Integer getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(Integer idReserva) {
		this.idReserva = idReserva;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getRutReserva() {
		return rutReserva;
	}

	public void setRutReserva(String rutReserva) {
		this.rutReserva = rutReserva;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Reserva [idReserva=" + idReserva + ", fechaInicio=" + fechaInicio + ", fechaTermino=" + fechaTermino
				+ ", rutReserva=" + rutReserva + ", mesa=" + mesa + ", usuario=" + usuario + "]";
	}

	
}