package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PAGO_PEDIDO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPagoPedidos",
		procedureName="PACK_PAGO_PEDIDO.GET",
		resultClasses = PagoPedido.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createPagoPedido",
		procedureName="PACK_PAGO_PEDIDO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fecha", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_descripcion", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updatePagoPedido",
	procedureName="PACK_PAGO_PEDIDO.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpago", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fecha", type=Date.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_descripcion", type=String.class),
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deletePagoPedido",
	procedureName="PACK_PAGO_PEDIDO.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpago", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class PagoPedido {
	
	//Atributos
	@Column(name = "IDPAGPED")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPagoPedido;
	
	@Column(name = "FECHAPAGPED", nullable = false)
	private Date fechaPagoPedido;
	
	@Column(name = "MONTOPAGPED", nullable = false, length = 30)
	private Integer montoPagoPedido;
	
	@Column(name = "DESCRIPCIONPAGPED", nullable = true, length = 50)
	private String descripcionPagoPedido;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPEDIDO")
    private Pedido pedido;
	
	//Constructores	
	protected PagoPedido() {
	}

	public PagoPedido(Integer idPagoPedido, Date fechaPagoPedido, Integer montoPagoPedido, String descripcionPagoPedido,
			Pedido pedido) {
		super();
		this.idPagoPedido = idPagoPedido;
		this.fechaPagoPedido = fechaPagoPedido;
		this.montoPagoPedido = montoPagoPedido;
		this.descripcionPagoPedido = descripcionPagoPedido;
		this.pedido = pedido;
	}

	public Integer getIdPagoPedido() {
		return idPagoPedido;
	}

	public void setIdPagoPedido(Integer idPagoPedido) {
		this.idPagoPedido = idPagoPedido;
	}

	public Date getFechaPagoPedido() {
		return fechaPagoPedido;
	}

	public void setFechaPagoPedido(Date fechaPagoPedido) {
		this.fechaPagoPedido = fechaPagoPedido;
	}

	public Integer getMontoPagoPedido() {
		return montoPagoPedido;
	}

	public void setMontoPagoPedido(Integer montoPagoPedido) {
		this.montoPagoPedido = montoPagoPedido;
	}

	public String getDescripcionPagoPedido() {
		return descripcionPagoPedido;
	}

	public void setDescripcionPagoPedido(String descripcionPagoPedido) {
		this.descripcionPagoPedido = descripcionPagoPedido;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public String toString() {
		return "PagoPedido [idPagoPedido=" + idPagoPedido + ", fechaPagoPedido=" + fechaPagoPedido
				+ ", montoPagoPedido=" + montoPagoPedido + ", descripcionPagoPedido=" + descripcionPagoPedido
				+ ", pedido=" + pedido + "]";
	}
	
	
}