package com.xxi.api.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


@Entity
@Table(name = "ESTADO_MESA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listEstadosMesa",
		procedureName="PACK_ESTADO_MESA.GET",
		resultClasses = EstadoMesa.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createEstadoMesa",
		procedureName="PACK_ESTADO_MESA.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updateEstadoMesa",
	procedureName="PACK_ESTADO_MESA.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deleteEstadoMesa",
	procedureName="PACK_ESTADO_MESA.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class EstadoMesa {

	@Column(name = "IDESTMESA")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idEstadoMesa;
	
	@Column(name = "NOMBREESTMESA", nullable = false, length = 50)
	private String nombreEstadoMesa;
	
	//Constructores
	protected EstadoMesa() {
	}

	public EstadoMesa(Integer idEstadoMesa, String nombreEstadoMesa) {
		super();
		this.idEstadoMesa = idEstadoMesa;
		this.nombreEstadoMesa = nombreEstadoMesa;
	}

	//Getters y Setters
	public Integer getIdEstadoMesa() {
		return idEstadoMesa;
	}

	public void setIdEstadoMesa(Integer idEstadoMesa) {
		this.idEstadoMesa = idEstadoMesa;
	}

	public String getNombreEstadoMesa() {
		return nombreEstadoMesa;
	}

	public void setNombreEstadoMesa(String nombreEstadoMesa) {
		this.nombreEstadoMesa = nombreEstadoMesa;
	}

	@Override
	public String toString() {
		return "EstadoMesa [idEstadoMesa=" + idEstadoMesa + ", nombreEstadoMesa=" + nombreEstadoMesa + "]";
	}
	
}