package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.ParameterMode;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORIA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listCategorias",
			procedureName="PACK_CATEGORIA.GET",
			resultClasses = Categoria.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createCategoria",
			procedureName="PACK_CATEGORIA.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateCategoria",
	procedureName="PACK_CATEGORIA.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delCategoria",
	procedureName="PACK_CATEGORIA.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class Categoria {

	@Column(name = "IDCAT")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCategoria;
	@Column(name = "NOMBRECAT", nullable = false, length = 50)
	private String nombreCategoria;
	
	//Constructor
	public Categoria(Integer idCategoria, String nombreCategoria) {
		super();
		this.idCategoria = idCategoria;
		this.nombreCategoria = nombreCategoria;
	}
	
	protected Categoria() {
		
	}
	
	//Getters & Setters
	public Integer getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getNombreCategoria() {
		return nombreCategoria;
	}
	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	//toString();
	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nombreCategoria=" + nombreCategoria + "]";
	}
	
}

