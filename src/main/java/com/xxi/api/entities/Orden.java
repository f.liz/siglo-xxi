package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listOrdenes",
		procedureName="PACK_ORDEN.GET",
		resultClasses = Orden.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createOrden",
		procedureName="PACK_ORDEN.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechaentrega", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_total", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idtipo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmesa", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateOrden",
	procedureName="PACK_ORDEN.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fechaentrega", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_total", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idtipo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmesa", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteOrden",
	procedureName="PACK_ORDEN.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Orden {
	
	//Atributos
	@Column(name = "IDOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idOrden;
	
	@Column(name = "FECHAINIOR", nullable = false)
	private Date fechaIni;
	
	@Column(name = "FECHAENTOR", nullable = false)
	private Date fechaEnt;
	
	@Column(name = "TOTALOR", nullable = false, length = 30)
	private Integer totalOrden;
	
	@Column(name = "ESTADOOR", nullable = false, length = 50)
	private String estadoOrden;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDTIPOR")
    private TipoOrden tipo;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMESA")
    private Mesa mesa;
	
	//Constructores	
	protected Orden() {
	}

	public Orden(Integer idOrden, Date fechaIni, Date fechaEnt, Integer totalOrden, String estadoOrden, TipoOrden tipo,
			Mesa mesa) {
		super();
		this.idOrden = idOrden;
		this.fechaIni = fechaIni;
		this.fechaEnt = fechaEnt;
		this.totalOrden = totalOrden;
		this.estadoOrden = estadoOrden;
		this.tipo = tipo;
		this.mesa = mesa;
	}

	public Integer getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Integer idOrden) {
		this.idOrden = idOrden;
	}

	public Date getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}

	public Date getFechaEnt() {
		return fechaEnt;
	}

	public void setFechaEnt(Date fechaEnt) {
		this.fechaEnt = fechaEnt;
	}

	public Integer getTotalOrden() {
		return totalOrden;
	}

	public void setTotalOrden(Integer totalOrden) {
		this.totalOrden = totalOrden;
	}

	public String getEstadoOrden() {
		return estadoOrden;
	}

	public void setEstadoOrden(String estadoOrden) {
		this.estadoOrden = estadoOrden;
	}

	public TipoOrden getTipo() {
		return tipo;
	}

	public void setTipo(TipoOrden tipo) {
		this.tipo = tipo;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	@Override
	public String toString() {
		return "Orden [idOrden=" + idOrden + ", fechaIni=" + fechaIni + ", fechaEnt=" + fechaEnt + ", totalOrden="
				+ totalOrden + ", estadoOrden=" + estadoOrden + ", tipo=" + tipo + ", mesa=" + mesa + "]";
	}

	
	
}