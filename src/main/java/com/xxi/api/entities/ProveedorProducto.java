package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PROVEEDOR_PRODUCTO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listProveedorProductos",
		procedureName="PACK_PROVEEDOR_PRODUCTO.GET",
		resultClasses = ProveedorProducto.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createProveedorProducto",
		procedureName="PACK_PROVEEDOR_PRODUCTO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_valor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproveedor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproducto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateProveedorProducto",
	procedureName="PACK_PROVEEDOR_PRODUCTO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprovprod", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_valor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproveedor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproducto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteProveedorProducto",
	procedureName="PACK_PROVEEDOR_PRODUCTO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprovprod", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class ProveedorProducto {
	
	//Atributos
	@Column(name = "IDPROVPROD")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idProvProd;

	@Column(name = "VALORPROD", nullable = false, length = 30)
	private Integer valorProducto;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPROV")
    private Proveedor proveedor;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPROD")
    private Producto producto;
	
	//Constructores	
	protected ProveedorProducto() {
	}

	public ProveedorProducto(Integer idProvProd, Integer valorProducto, Proveedor proveedor, Producto producto) {
		super();
		this.idProvProd = idProvProd;
		this.valorProducto = valorProducto;
		this.proveedor = proveedor;
		this.producto = producto;
	}

	public Integer getIdProvProd() {
		return idProvProd;
	}

	public void setIdProvProd(Integer idProvProd) {
		this.idProvProd = idProvProd;
	}

	public Integer getValorProducto() {
		return valorProducto;
	}

	public void setValorProducto(Integer valorProducto) {
		this.valorProducto = valorProducto;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "ProveedorProducto [idProvProd=" + idProvProd + ", valorProducto=" + valorProducto + ", proveedor="
				+ proveedor + ", producto=" + producto + "]";
	}
	
}