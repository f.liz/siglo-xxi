package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PLATO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPlatos",
		procedureName="PACK_PLATO.GET",
		resultClasses = Plato.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createPlato",
		procedureName="PACK_PLATO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_valor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_tiempo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_preparacion", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idcategoria", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updatePlato",
	procedureName="PACK_PLATO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_valor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_tiempo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_preparacion", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idcategoria", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deletePlato",
	procedureName="PACK_PLATO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Plato {
	
	//Atributos
	@Column(name = "IDPLATO")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPlato;
	
	@Column(name = "NOMBREPLATO", nullable = false, length = 50)
	private String nombrePlato;
	
	@Column(name = "VALORPLATO", nullable = false, length = 30)
	private Integer valorPlato;
	
	@Column(name = "TIEMPOPREPPLATO", nullable = false, length = 30)
	private Integer tiempoPlato;
	
	@Column(name = "PREPPLATO", nullable = false, length = 3000)
	private String prepPlato;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCAT")
    private Categoria categoria;
	
	//Constructores	
	protected Plato() {
	}

	public Plato(Integer idPlato, String nombrePlato, Integer valorPlato, Integer tiempoPlato, String prepPlato,
			Categoria categoria) {
		super();
		this.idPlato = idPlato;
		this.nombrePlato = nombrePlato;
		this.valorPlato = valorPlato;
		this.tiempoPlato = tiempoPlato;
		this.prepPlato = prepPlato;
		this.categoria = categoria;
	}

	public Integer getIdPlato() {
		return idPlato;
	}

	public void setIdPlato(Integer idPlato) {
		this.idPlato = idPlato;
	}

	public String getNombrePlato() {
		return nombrePlato;
	}

	public void setNombrePlato(String nombrePlato) {
		this.nombrePlato = nombrePlato;
	}

	public Integer getValorPlato() {
		return valorPlato;
	}

	public void setValorPlato(Integer valorPlato) {
		this.valorPlato = valorPlato;
	}

	public Integer getTiempoPlato() {
		return tiempoPlato;
	}

	public void setTiempoPlato(Integer tiempoPlato) {
		this.tiempoPlato = tiempoPlato;
	}

	public String getPrepPlato() {
		return prepPlato;
	}

	public void setPrepPlato(String prepPlato) {
		this.prepPlato = prepPlato;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return "Plato [idPlato=" + idPlato + ", nombrePlato=" + nombrePlato + ", valorPlato=" + valorPlato
				+ ", tiempoPlato=" + tiempoPlato + ", prepPlato=" + prepPlato + ", categoria=" + categoria + "]";
	}

	

	
	
}