package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PAGO_ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPagoOrdenes",
		procedureName="PACK_PAGO_ORDEN.GET",
		resultClasses = PagoOrden.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createPagoOrden",
		procedureName="PACK_PAGO_ORDEN.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_propina", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmetodo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updatePagoOrden",
	procedureName="PACK_PAGO_ORDEN.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpago", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_fecha", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_monto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_propina", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idmetodo", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deletePagoOrden",
	procedureName="PACK_PAGO_ORDEN.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpago", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class PagoOrden {
	
	//Atributos
	@Column(name = "IDPAGOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPagOr;

	@Column(name = "FECHAPAGOR", nullable = false, length = 30)
	private Date fechaPagOr;
	
	@Column(name = "MONTOPAGOR", nullable = false, length = 30)
	private Integer montoPagOr;
	
	@Column(name = "PROPINAPAGOR", nullable = true, length = 30)
	private Integer propinaPagOr;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDOR")
    private Orden orden;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMETPAG")
    private MetodoPago metodoPago;
	
	//Constructores	
	protected PagoOrden() {
	}

	public PagoOrden(Integer idPagOr, Date fechaPagOr, Integer montoPagOr, Integer propinaPagOr, Orden orden,
			MetodoPago metodoPago) {
		super();
		this.idPagOr = idPagOr;
		this.fechaPagOr = fechaPagOr;
		this.montoPagOr = montoPagOr;
		this.propinaPagOr = propinaPagOr;
		this.orden = orden;
		this.metodoPago = metodoPago;
	}

	public Integer getIdPagOr() {
		return idPagOr;
	}

	public void setIdPagOr(Integer idPagOr) {
		this.idPagOr = idPagOr;
	}

	public Date getFechaPagOr() {
		return fechaPagOr;
	}

	public void setFechaPagOr(Date fechaPagOr) {
		this.fechaPagOr = fechaPagOr;
	}

	public Integer getMontoPagOr() {
		return montoPagOr;
	}

	public void setMontoPagOr(Integer montoPagOr) {
		this.montoPagOr = montoPagOr;
	}

	public Integer getPropinaPagOr() {
		return propinaPagOr;
	}

	public void setPropinaPagOr(Integer propinaPagOr) {
		this.propinaPagOr = propinaPagOr;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public MetodoPago getMetodoPago() {
		return metodoPago;
	}

	public void setMetodoPago(MetodoPago metodoPago) {
		this.metodoPago = metodoPago;
	}

	@Override
	public String toString() {
		return "PagoOrden [idPagOr=" + idPagOr + ", fechaPagOr=" + fechaPagOr + ", montoPagOr=" + montoPagOr
				+ ", propinaPagOr=" + propinaPagOr + ", orden=" + orden + ", metodoPago=" + metodoPago + "]";
	}

	
}