package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "DETALLE_ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listDetalleOrdenes",
		procedureName="PACK_DETALLE_PEDIDO.GET",
		resultClasses = DetalleOrden.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createDetalleOrden",
		procedureName="PACK_DETALLE_ORDEN.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_orden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateDetalleOrden",
	procedureName="PACK_DETALLE_ORDEN.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_iddetor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_orden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteDetalleOrden",
	procedureName="PACK_DETALLE_ORDEN.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_iddetor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class DetalleOrden {
	
	//Atributos
	@Column(name = "IDDETOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idDetOr;

	@Column(name = "CANTIDADDETOR", nullable = false, length = 30)
	private Integer cantidadDetOr;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPLATO")
    private Plato plato;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTPLAT")
    private EstadoPlato estadoPlato;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDOR")
    private Orden orden;
	
	//Constructores	
	protected DetalleOrden() {
	}

	public DetalleOrden(Integer idDetOr, Integer cantidadDetOr, Plato plato, EstadoPlato estadoPlato, Orden orden) {
		super();
		this.idDetOr = idDetOr;
		this.cantidadDetOr = cantidadDetOr;
		this.plato = plato;
		this.estadoPlato = estadoPlato;
		this.orden = orden;
	}

	public Integer getIdDetOr() {
		return idDetOr;
	}

	public void setIdDetOr(Integer idDetOr) {
		this.idDetOr = idDetOr;
	}

	public Integer getCantidadDetOr() {
		return cantidadDetOr;
	}

	public void setCantidadDetOr(Integer cantidadDetOr) {
		this.cantidadDetOr = cantidadDetOr;
	}

	public Plato getPlato() {
		return plato;
	}

	public void setPlato(Plato plato) {
		this.plato = plato;
	}

	public EstadoPlato getEstadoPlato() {
		return estadoPlato;
	}

	public void setEstadoPlato(EstadoPlato estadoPlato) {
		this.estadoPlato = estadoPlato;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	@Override
	public String toString() {
		return "DetalleOrden [idDetOr=" + idDetOr + ", cantidadDetOr=" + cantidadDetOr + ", plato=" + plato
				+ ", estadoPlato=" + estadoPlato + ", orden=" + orden + "]";
	}

	
}