package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "ESTADO_PLATO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listEstadosPlato",
		procedureName="PACK_ESTADO_PLATO.GET",
		resultClasses = EstadoPlato.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createEstadoPlato",
		procedureName="PACK_ESTADO_PLATO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updateEstadoPlato",
	procedureName="PACK_ESTADO_PLATO.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deleteEstadoPlato",
	procedureName="PACK_ESTADO_PLATO.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class EstadoPlato {

	@Column(name = "IDESTPLAT")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idEstadoPlato;
	
	@Column(name = "NOMBREESTPLAT", nullable = false, length = 50)
	private String nombreEstadoPlato;
	
	//Constructores
	protected EstadoPlato() {
	}

	public EstadoPlato(Integer idEstadoPlato, String nombreEstadoPlato) {
		super();
		this.idEstadoPlato = idEstadoPlato;
		this.nombreEstadoPlato = nombreEstadoPlato;
	}

	//Getters y Setters
	public Integer getIdEstadoPlato() {
		return idEstadoPlato;
	}

	public void setIdEstadoPlato(Integer idEstadoPlato) {
		this.idEstadoPlato = idEstadoPlato;
	}

	public String getNombreEstadoPlato() {
		return nombreEstadoPlato;
	}

	public void setNombreEstadoPlato(String nombreEstadoPlato) {
		this.nombreEstadoPlato = nombreEstadoPlato;
	}

	@Override
	public String toString() {
		return "EstadoPlato [idEstadoPlato=" + idEstadoPlato + ", nombreEstadoPlato=" + nombreEstadoPlato + "]";
	}
}
