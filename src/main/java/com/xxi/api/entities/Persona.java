package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "PERSONA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPersonas",
		procedureName="PACK_PERSONA.GET",
		resultClasses = Usuario.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createPersona",
		procedureName="PACK_PERSONA.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_rut", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_apellido", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updatePersona",
	procedureName="PACK_PERSONA.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpersona", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_rut", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_apellido", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deletePersona",
	procedureName="PACK_PERSONA.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpersona", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Persona {
	
	//Atributos
	@Column(name = "IDPERSON")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPerson;
	
	@Column(name = "RUTPERSON", nullable = false, length = 50)
	private String rutPerson;
	
	@Column(name = "NOMBREPERSON", nullable = false, length = 255)
	private String nombrePerson;
	
	@Column(name = "APELLIDOPERSON", nullable = false, length = 1)
	private String apellidoPerson;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDUSUARIO")
    private Usuario usuario;
	
	//Constructores	
	protected Persona() {
	}

	public Persona(Integer idPerson, String rutPerson, String nombrePerson, String apellidoPerson, Usuario usuario) {
		super();
		this.idPerson = idPerson;
		this.rutPerson = rutPerson;
		this.nombrePerson = nombrePerson;
		this.apellidoPerson = apellidoPerson;
		this.usuario = usuario;
	}

	public Integer getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(Integer idPerson) {
		this.idPerson = idPerson;
	}

	public String getRutPerson() {
		return rutPerson;
	}

	public void setRutPerson(String rutPerson) {
		this.rutPerson = rutPerson;
	}

	public String getNombrePerson() {
		return nombrePerson;
	}

	public void setNombrePerson(String nombrePerson) {
		this.nombrePerson = nombrePerson;
	}

	public String getApellidoPerson() {
		return apellidoPerson;
	}

	public void setApellidoPerson(String apellidoPerson) {
		this.apellidoPerson = apellidoPerson;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Persona [idPerson=" + idPerson + ", rutPerson=" + rutPerson + ", nombrePerson=" + nombrePerson
				+ ", apellidoPerson=" + apellidoPerson + ", usuario=" + usuario + "]";
	}

		
}