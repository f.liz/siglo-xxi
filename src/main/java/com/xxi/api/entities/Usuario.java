package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "USUARIO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listUsuarios",
		procedureName="PACK_USUARIO.GET",
		resultClasses = Usuario.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createUsuario",
		procedureName="PACK_USUARIO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_email", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_pass", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_perfil", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateUsuario",
	procedureName="PACK_USUARIO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_email", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_pass", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_perfil", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteUsuario",
	procedureName="PACK_USUARIO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Usuario {
	
	//Atributos
	@Column(name = "IDUSUARIO")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idUsuario;
	
	@Column(name = "EMAILUSUARIO", nullable = false, length = 50)
	private String emailUsuario;
	
	@Column(name = "PASSUSUARIO", nullable = false, length = 255)
	private String passUsuario;
	
	@Column(name = "ESTADOUSUARIO", nullable = false, length = 1)
	private String estadoUsuario;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPERFIL")
    private Perfil perfil;
	
	//Constructores	
	protected Usuario() {
	}

	public Usuario(Integer idUsuario, String emailUsuario, String passUsuario, String estadoUsuario, Perfil perfil) {
		super();
		this.idUsuario = idUsuario;
		this.emailUsuario = emailUsuario;
		this.passUsuario = passUsuario;
		this.estadoUsuario = estadoUsuario;
		this.perfil = perfil;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public String getPassUsuario() {
		return passUsuario;
	}

	public void setPassUsuario(String passUsuario) {
		this.passUsuario = passUsuario;
	}

	public String getEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(String estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", emailUsuario=" + emailUsuario + ", passUsuario=" + passUsuario
				+ ", estadoUsuario=" + estadoUsuario + ", perfil=" + perfil + "]";
	}

	
	
}