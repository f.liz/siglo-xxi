package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "PROVEEDOR") 
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listProveedores",
			procedureName="PACK_PROVEEDOR.GET",
			resultClasses = MovimientoDinero.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createProveedor",
			procedureName="PACK_PROVEEDOR.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_email", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_telefono", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updateProveedor",
	procedureName="PACK_PROVEEDOR.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprov", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_email", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_telefono", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_estado", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delProveedor",
	procedureName="PACK_PROVEEDOR.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprov", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class Proveedor {

	@Column(name = "IDPROV")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idProveedor;
	
	@Column(name = "NOMBREPROV", nullable = false, length = 50)
	private String nombreProveedor ;
	
	@Column(name = "EMAILPROV", nullable = false, length = 50)
	private String emailProveedor;
	
	@Column(name = "TELEFONOPROV", nullable = false, length = 15)
	private String telefonoProveedor;
	
	@Column(name = "ESTADOPROV", nullable = false, length = 1)
	private String estadoProveedor;
	
	//Constructores	
	protected Proveedor() {
	}

	public Proveedor(Integer idProveedor, String nombreProveedor, String emailProveedor, String telefonoProveedor,
			String estadoProveedor) {
		super();
		this.idProveedor = idProveedor;
		this.nombreProveedor = nombreProveedor;
		this.emailProveedor = emailProveedor;
		this.telefonoProveedor = telefonoProveedor;
		this.estadoProveedor = estadoProveedor;
	}

	public Integer getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getEmailProveedor() {
		return emailProveedor;
	}

	public void setEmailProveedor(String emailProveedor) {
		this.emailProveedor = emailProveedor;
	}

	public String getTelefonoProveedor() {
		return telefonoProveedor;
	}

	public void setTelefonoProveedor(String telefonoProveedor) {
		this.telefonoProveedor = telefonoProveedor;
	}

	public String getEstadoProveedor() {
		return estadoProveedor;
	}

	public void setEstadoProveedor(String estadoProveedor) {
		this.estadoProveedor = estadoProveedor;
	}

	@Override
	public String toString() {
		return "Proveedor [idProveedor=" + idProveedor + ", nombreProveedor=" + nombreProveedor + ", emailProveedor="
				+ emailProveedor + ", telefonoProveedor=" + telefonoProveedor + ", estadoProveedor=" + estadoProveedor
				+ "]";
	}
	
}
