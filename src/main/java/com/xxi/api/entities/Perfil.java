package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "PERFIL")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listPerfiles",
			procedureName="PACK_PERFIL.GET",
			resultClasses = Perfil.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createPerfil",
			procedureName="PACK_PERFIL.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updatePerfil",
	procedureName="PACK_PERFIL.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delPerfil",
	procedureName="PACK_PERFIL.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class Perfil {
	
	@Column(name = "IDPERFIL")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idPerfil;
	@Column(name = "NOMBREPERFIL", nullable = false, length = 50)
	private String nombrePerfil;
	
	//Constructor
	public Perfil(Integer idPerfil, String nombrePerfil) {
		super();
		//this.usuarios = new HashSet<>();
		this.idPerfil = idPerfil;
		this.nombrePerfil = nombrePerfil;
	}
	
	protected Perfil() {
		//this.usuarios = new HashSet<>();
	}
	
	//Getters & Setters
	public Integer getIdPerfil() {
		return idPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}
	public String getNombrePerfil() {
		return nombrePerfil;
	}
	public void setNombrePerfil(String nombrePerfil) {
		this.nombrePerfil = nombrePerfil;
	}
	
	//toString();
	@Override
	public String toString() {
		return "Perfil [idPerfil=" + idPerfil + ", nombrePerfil=" + nombrePerfil + "]";
	}
	
}