package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "RECETA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listRecetas",
		procedureName="PACK_RECETA.GET",
		resultClasses = Receta.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createReceta",
		procedureName="PACK_RECETA.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproducto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateReceta",
	procedureName="PACK_RECETA.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idreceta", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idproducto", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idplato", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteReceta",
	procedureName="PACK_RECETA.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idreceta", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class Receta {
	
	//Atributos
	@Column(name = "IDRECETA")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idReceta;

	@Column(name = "CANTIDADRECETA", nullable = false, precision=30, scale=2)
	private Double cantidadReceta;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPROD")
    private Producto producto;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPLATO")
    private Plato plato;
	
	//Constructores	
	protected Receta() {
	}

	public Receta(Integer idReceta, Double cantidadReceta, Producto producto, Plato plato) {
		super();
		this.idReceta = idReceta;
		this.cantidadReceta = cantidadReceta;
		this.producto = producto;
		this.plato = plato;
	}

	public Integer getIdReceta() {
		return idReceta;
	}

	public void setIdReceta(Integer idReceta) {
		this.idReceta = idReceta;
	}

	public Double getCantidadReceta() {
		return cantidadReceta;
	}

	public void setCantidadReceta(Double cantidadReceta) {
		this.cantidadReceta = cantidadReceta;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Plato getPlato() {
		return plato;
	}

	public void setPlato(Plato plato) {
		this.plato = plato;
	}

	@Override
	public String toString() {
		return "Receta [idReceta=" + idReceta + ", cantidadReceta=" + cantidadReceta + ", producto=" + producto
				+ ", plato=" + plato + "]";
	}


	
}