package com.xxi.api.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


@Entity
@Table(name = "ESTADO_ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listEstadosOrden",
		procedureName="PACK_ESTADO_ORDEN.GET",
		resultClasses = EstadoOrden.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createEstadoOrden",
		procedureName="PACK_ESTADO_ORDEN.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="updateEstadoOrden",
	procedureName="PACK_ESTADO_ORDEN.PUT",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	}),
	@NamedStoredProcedureQuery(name="deleteEstadoOrden",
	procedureName="PACK_ESTADO_ORDEN.DEL",
	parameters = {
		@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),	
		@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class EstadoOrden {

	@Column(name = "IDESTOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idEstadoOrden;
	
	@Column(name = "NOMBREESTOR", nullable = false, length = 50)
	private String nombreEstadoOrden;
	
	//Constructores
	protected EstadoOrden() {
	}

	public EstadoOrden(Integer idEstadoOrden, String nombreEstadoOrden) {
		super();
		this.idEstadoOrden = idEstadoOrden;
		this.nombreEstadoOrden = nombreEstadoOrden;
	}

	//Getters y Setters
	public Integer getIdEstadoOrden() {
		return idEstadoOrden;
	}

	public void setIdEstadoOrden(Integer idEstadoOrden) {
		this.idEstadoOrden = idEstadoOrden;
	}

	public String getNombreEstadoOrden() {
		return nombreEstadoOrden;
	}

	public void setNombreEstadoMesa(String nombreEstadoOrden) {
		this.nombreEstadoOrden = nombreEstadoOrden;
	}

	@Override
	public String toString() {
		return "EstadoOrden [idEstadoOrden=" + idEstadoOrden + ", nombreEstadoOrden=" + nombreEstadoOrden + "]";
	}
	
}