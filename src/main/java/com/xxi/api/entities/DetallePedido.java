package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "DETALLE_PEDIDO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listDetallePedidos",
		procedureName="PACK_DETALLE_PEDIDO.GET",
		resultClasses = DetallePedido.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createDetallePedido",
		procedureName="PACK_DETALLE_PEDIDO.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprovprod", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateDetallePedido",
	procedureName="PACK_DETALLE_PEDIDO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_iddetped", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cantidad", type=Double.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idprovprod", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idpedido", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteDetallePedido",
	procedureName="PACK_DETALLE_PEDIDO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_iddetped", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class DetallePedido {
	
	//Atributos
	@Column(name = "IDDETPED")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idDetPed;

	@Column(name = "CANTIDADDETPED", nullable = false, precision=30, scale=2)
	private Double cantidadDetPed;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPROVPROD")
    private ProveedorProducto provProd;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPEDIDO")
    private Pedido pedido;
	
	//Constructores	
	protected DetallePedido() {
	}

	public DetallePedido(Integer idDetPed, Double cantidadDetPed, ProveedorProducto provProd, Pedido pedido) {
		super();
		this.idDetPed = idDetPed;
		this.cantidadDetPed = cantidadDetPed;
		this.provProd = provProd;
		this.pedido = pedido;
	}

	public Integer getIdDetPed() {
		return idDetPed;
	}

	public void setIdDetPed(Integer idDetPed) {
		this.idDetPed = idDetPed;
	}

	public Double getCantidadDetPed() {
		return cantidadDetPed;
	}

	public void setCantidadDetPed(Double cantidadDetPed) {
		this.cantidadDetPed = cantidadDetPed;
	}

	public ProveedorProducto getProvProd() {
		return provProd;
	}

	public void setProvProd(ProveedorProducto provProd) {
		this.provProd = provProd;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public String toString() {
		return "DetallePedido [idDetPed=" + idDetPed + ", cantidadDetPed=" + cantidadDetPed + ", provProd=" + provProd
				+ ", pedido=" + pedido + "]";
	}

	
}