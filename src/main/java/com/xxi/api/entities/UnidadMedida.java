package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "UNIDAD_MEDIDA")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listUnidades",
			procedureName="PACK_UNIDAD_MEDIDA.GET",
			resultClasses = UnidadMedida.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createUnidad",
			procedureName="PACK_UNIDAD_MEDIDA.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cod", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updateUnidad",
	procedureName="PACK_UNIDAD_MEDIDA.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_cod", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delUnidad",
	procedureName="PACK_UNIDAD_MEDIDA.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class UnidadMedida {

	@Column(name = "IDUNIMED")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idUnidadMedida;
	@Column(name = "CODIGOUNIMED", nullable = false, length = 15)
	private String codigoUnidadMedida;
	@Column(name = "NOMBREUNIMED", nullable = false, length = 50)
	private String nombreUnidadMedida;
	
	//Constructor
	public UnidadMedida(Integer idUnidadMedida, String codigoUnidadMedida, String nombreUnidadMedida) {
		super();
		this.idUnidadMedida = idUnidadMedida;
		this.codigoUnidadMedida = codigoUnidadMedida;
		this.nombreUnidadMedida = nombreUnidadMedida;
	}
	
	protected UnidadMedida() {
		
	}
	
	//Getters & Setters
	public Integer getIdUnidadMedida() {
		return idUnidadMedida;
	}
	public void setIdUnidadMedida(Integer idUnidadMedida) {
		this.idUnidadMedida = idUnidadMedida;
	}
	public String getCodigoUnidadMedida() {
		return codigoUnidadMedida;
	}
	public void setCodigoUnidadMedida(String codigoUnidadMedida) {
		this.codigoUnidadMedida = codigoUnidadMedida;
	}
	public String getNombreUnidadMedida() {
		return nombreUnidadMedida;
	}
	public void setNombreUnidadMedida(String nombreUnidadMedida) {
		this.nombreUnidadMedida = nombreUnidadMedida;
	}
	
	//toString();
	@Override
	public String toString() {
		return "UnidadMedida [idUnidadMedida=" + idUnidadMedida + ", codigoUnidadMedida=" + codigoUnidadMedida
				+ ", nombreUnidadMedida=" + nombreUnidadMedida + "]";
	}
	
}
