package com.xxi.api.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
@Table(name = "METODO_PAGO")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listMetodosPago",
			procedureName="PACK_METODO_PAGO.GET",
			resultClasses = MetodoPago.class,
			parameters = {
				@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
		}),
	@NamedStoredProcedureQuery(name="createMetodo",
			procedureName="PACK_METODO_PAGO.POST",
			parameters = {
					@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
					@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
			}),
	@NamedStoredProcedureQuery(name="updateMetodo",
	procedureName="PACK_METODO_PAGO.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_nombre", type=String.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="delMetodo",
	procedureName="PACK_METODO_PAGO.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_id", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	})
})
public class MetodoPago {

	@Column(name = "IDMETPAG")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idMetodoPago;
	@Column(name = "NOMBREMETPAG", nullable = false, length = 50)
	private String nombreMetodoPago ;
	
	//Constructor
	public MetodoPago(Integer idMetodoPago, String nombreMetodoPago) {
		super();
		this.idMetodoPago = idMetodoPago;
		this.nombreMetodoPago = nombreMetodoPago;
	}
	
	protected MetodoPago() {
		
	}
	
	//Getters & Setters
	public Integer getIdMetodoPago() {
		return idMetodoPago;
	}
	public void setIdMetodoPago(Integer idMetodoPago) {
		this.idMetodoPago = idMetodoPago;
	}
	public String getNombreMetodoPago() {
		return nombreMetodoPago;
	}
	public void setNombreMetodoPago(String nombreMetodoPago) {
		this.nombreMetodoPago = nombreMetodoPago;
	}
	
	//toString();
	@Override
	public String toString() {
		return "MetodoPago [idMetodoPago=" + idMetodoPago + ", nombreMetodoPago=" + nombreMetodoPago + "]";
	}
	
}
