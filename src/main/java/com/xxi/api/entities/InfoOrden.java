package com.xxi.api.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "INFO_ORDEN")
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name="listInfoOrdenes",
		procedureName="PACK_INFO_ORDEN.GET",
		resultClasses = InfoOrden.class,
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.REF_CURSOR, name="cursor", type=void.class)
	}),
	@NamedStoredProcedureQuery(name="createInfoOrden",
		procedureName="PACK_INFO_ORDEN.POST",
		parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_hora", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idestado", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_created", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="updateInfoOrden",
	procedureName="PACK_INFO_ORDEN.PUT",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idinfor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_hora", type=Date.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idorden", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idestado", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idusuario", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class)
	}),
	@NamedStoredProcedureQuery(name="deleteInfoOrden",
	procedureName="PACK_INFO_ORDEN.DEL",
	parameters = {
			@StoredProcedureParameter(mode=ParameterMode.IN, name="v_idinfor", type=Integer.class),
			@StoredProcedureParameter(mode=ParameterMode.OUT, name="v_rows", type=Integer.class),
	})
})
public class InfoOrden {
	
	//Atributos
	@Column(name = "IDINFOR")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idInfOr;

	@Column(name = "REGISTROHORA", nullable = false)
	private Date registroHora;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDOR")
    private Orden orden;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTOR")
    private EstadoOrden estadoOrden;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDUSUARIO")
    private Usuario usuario;
	
	//Constructores	
	protected InfoOrden() {
	}

	public InfoOrden(Integer idDetOr, Date registroHora, Orden orden, EstadoOrden estadoOrden, Usuario usuario) {
		super();
		this.idInfOr = idDetOr;
		this.registroHora = registroHora;
		this.orden = orden;
		this.estadoOrden = estadoOrden;
		this.usuario = usuario;
	}

	public Integer getIdInfOr() {
		return idInfOr;
	}

	public void setIdInfOr(Integer idInfOr) {
		this.idInfOr = idInfOr;
	}

	public Date getRegistroHora() {
		return registroHora;
	}

	public void setRegistroHora(Date registroHora) {
		this.registroHora = registroHora;
	}

	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public EstadoOrden getEstadoOrden() {
		return estadoOrden;
	}

	public void setEstadoOrden(EstadoOrden estadoOrden) {
		this.estadoOrden = estadoOrden;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "InfoOrden [idDetOr=" + idInfOr + ", cantidadDetOr=" + registroHora + ", orden=" + orden
				+ ", estadoOrden=" + estadoOrden + ", usuario=" + usuario + "]";
	}

}