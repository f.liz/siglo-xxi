package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.DetalleOrden;
import com.xxi.api.repositories.DetalleOrdenDAO;




@Service
public class DetalleOrdenService {

	@Autowired
	DetalleOrdenDAO d;
	
	public List<DetalleOrden> getAll() {
		
		return this.d.findAll();
	}
	
	public Optional<DetalleOrden> findOne(Integer idDetOr){
		
		return this.d.findById(idDetOr);
	}
	
	public DetalleOrden saveOne(DetalleOrden det) {
		
		Integer detId = this.d.create(det.getCantidadDetOr(), det.getPlato().getIdPlato(), det.getEstadoPlato().getIdEstadoPlato(), det.getOrden().getIdOrden());
		if (detId > 0) {
			det.setIdDetOr(detId);
		}
		return det;
	}
	
	public Integer updateOne(DetalleOrden det, Integer idDetOr) {

			Integer filas = this.d.update(idDetOr, det.getCantidadDetOr(), det.getPlato().getIdPlato(), det.getEstadoPlato().getIdEstadoPlato(), det.getOrden().getIdOrden());
			return filas;
	}
	
	public Integer deleteOne(Integer idDetOr) {
		
		return this.d.del(idDetOr);
	}
	
}
