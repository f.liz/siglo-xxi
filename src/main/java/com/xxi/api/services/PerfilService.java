package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Perfil;
import com.xxi.api.repositories.PerfilDAO;



@Service
public class PerfilService {

	@Autowired
	PerfilDAO p;
	
	public List<Perfil> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Perfil> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Perfil saveOne(Perfil per) {
		
		Integer perId = this.p.create(per.getNombrePerfil());
		per.setIdPerfil(perId);
		return per;
	}
	
	public Integer updateOne(Perfil per, Integer id) {
		
		if(per.getIdPerfil() == id) {
			return this.p.update(id, per.getNombrePerfil());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
}
