package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Plato;
import com.xxi.api.repositories.PlatoDAO;


@Service
public class PlatoService {

	@Autowired
	PlatoDAO p;
	
	public List<Plato> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Plato> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Plato saveOne(Plato pla) {
		
		Integer usId = this.p.create(pla.getNombrePlato(), pla.getValorPlato(), pla.getTiempoPlato(), pla.getPrepPlato(), pla.getCategoria().getIdCategoria());
		pla.setIdPlato(usId);
		return pla;
	}
	
	public Integer updateOne(Plato pla, Integer id) {

			Integer filas = this.p.update(id, pla.getNombrePlato(), pla.getValorPlato(), pla.getTiempoPlato(), pla.getPrepPlato(), pla.getCategoria().getIdCategoria());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
