package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Receta;
import com.xxi.api.repositories.RecetaDAO;



@Service
public class RecetaService {

	@Autowired
	RecetaDAO r;
	
	public List<Receta> getAll() {
		
		return this.r.findAll();
	}
	
	public Optional<Receta> findOne(Integer id){
		
		return this.r.findById(id);
	}
	
	public Receta saveOne(Receta re) {
		
		Integer reId = this.r.create(re.getCantidadReceta(), re.getProducto().getIdProducto(), re.getPlato().getIdPlato());
		if (reId > 0) {
			re.setIdReceta(reId);
		}
		return re;
	}
	
	public Integer updateOne(Receta re, Integer id) {

			Integer filas = this.r.update(id, re.getCantidadReceta(), re.getProducto().getIdProducto(), re.getPlato().getIdPlato());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.r.del(id);
	}
	
}
