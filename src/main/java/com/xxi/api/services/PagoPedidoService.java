package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.PagoPedido;
import com.xxi.api.repositories.PagoPedidoDAO;

@Service
public class PagoPedidoService {

	@Autowired
	PagoPedidoDAO p;
	
	public List<PagoPedido> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<PagoPedido> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public PagoPedido saveOne(PagoPedido pp) {
		
		Integer ppId = this.p.create(pp.getFechaPagoPedido(), pp.getMontoPagoPedido(), pp.getDescripcionPagoPedido(), pp.getPedido().getIdPedido());
		pp.setIdPagoPedido(ppId);
		return pp;
	}
	
	public Integer updateOne(PagoPedido pp, Integer id) {

			Integer filas = this.p.update(id, pp.getFechaPagoPedido(), pp.getMontoPagoPedido(), pp.getDescripcionPagoPedido(), pp.getPedido().getIdPedido());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
