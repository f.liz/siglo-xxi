package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.TipoOrden;
import com.xxi.api.repositories.TipoOrdenDAO;


@Service
public class TipoOrdenService {

	@Autowired
	TipoOrdenDAO to;
	
	public List<TipoOrden> getAll() {
		
		return this.to.findAll();
	}
	
	public Optional<TipoOrden> findOne(Integer id) {
		
		return this.to.findById(id);
	}
	
	public TipoOrden saveOne(TipoOrden tip) {
		
		return this.to.save(tip);
	}
	
	public Integer updateOne(TipoOrden tip, Integer id) {
		
		if(tip.getIdTipo()== id) {
			return this.to.update(id, tip.getNombreTipo());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.to.del(id);
	}
}
