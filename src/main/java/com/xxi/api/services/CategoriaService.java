package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Categoria;
import com.xxi.api.repositories.CategoriaDAO;


@Service
public class CategoriaService {
	
	@Autowired
	CategoriaDAO c;
	
	public List<Categoria> getAll() {
		
		return this.c.findAll();
	}
	
	public Optional<Categoria> findOne(Integer id) {
		
		return this.c.findById(id);
	}
	
	public Categoria saveOne(Categoria cat) {
		
		Integer catId = this.c.create(cat.getNombreCategoria());
		cat.setIdCategoria(catId);
		return cat;
	}
	
	public Integer updateOne(Categoria cat, Integer id) {
		
		if(cat.getIdCategoria() == id) {
			return this.c.update(id, cat.getNombreCategoria());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.c.del(id);
	}

}
