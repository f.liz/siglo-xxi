package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.EstadoOrden;
import com.xxi.api.repositories.EstadoOrdenDAO;

@Service
public class EstadoOrdenService {
	
	@Autowired
	EstadoOrdenDAO eo;
	
	public List<EstadoOrden> getAll() {
		
		return this.eo.findAll();
	}
	
	public Optional<EstadoOrden> findOne(Integer id) {
		
		return this.eo.findById(id);
	}
	
	public EstadoOrden saveOne(EstadoOrden est) {
		
		Integer estId = this.eo.create(est.getNombreEstadoOrden());
		est.setIdEstadoOrden(estId);
		return est;
	}
	
	public Integer updateOne(EstadoOrden est, Integer id) {
		
		if(est.getIdEstadoOrden() == id) {
			return this.eo.update(id, est.getNombreEstadoOrden());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.eo.del(id);
	}

}
