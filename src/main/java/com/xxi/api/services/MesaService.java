package com.xxi.api.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Mesa;
import com.xxi.api.repositories.MesaDAO;

@Service
public class MesaService {

	@Autowired
	MesaDAO m;
	
	public List<Mesa> getMesasReserva(Integer cap, Date fecha) {
		
		return this.m.getMesasReserva(cap, fecha);
	}
	
	public List<Mesa> getAll() {
		
		return this.m.findAll();
	}
	
	public Optional<Mesa> findOne(Integer id) {
		
		return this.m.findById(id);
	}
	
	public Mesa saveOne(Mesa me) {
		
		Integer meId = this.m.create(me.getNumeroMesa(), me.getCapacidadMesa(), me.getEstado().getIdEstadoMesa());
		me.setIdMesa(meId);
		return me;
	}
	
	public Integer updateOne(Mesa me, Integer id) {

			Integer filas = this.m.update(id, me.getNumeroMesa(), me.getCapacidadMesa(), me.getEstado().getIdEstadoMesa());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.m.del(id);
	}
	
}
