package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Producto;
import com.xxi.api.repositories.ProductoDAO;



@Service
public class ProductoService {

	@Autowired
	ProductoDAO p;
	
	public List<Producto> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Producto> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Producto saveOne(Producto pro) {
		
		Integer proId = this.p.create(pro.getNombreProducto(), pro.getStockProducto(), pro.getStockMinimo(), pro.getUnidad().getIdUnidadMedida());
		pro.setIdProducto(proId);
		return pro;
	}
	
	public Integer updateOne(Producto pro, Integer id) {

			Integer filas = this.p.update(id, pro.getNombreProducto(), pro.getStockProducto(), pro.getStockMinimo(), pro.getUnidad().getIdUnidadMedida());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
