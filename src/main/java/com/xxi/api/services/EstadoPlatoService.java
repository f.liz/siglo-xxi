package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.EstadoPlato;
import com.xxi.api.repositories.EstadoPlatoDAO;

@Service
public class EstadoPlatoService {
	
	@Autowired
	EstadoPlatoDAO ep;
	
	public List<EstadoPlato> getAll() {
		
		return this.ep.findAll();
	}
	
	public Optional<EstadoPlato> findOne(Integer id) {
		
		return this.ep.findById(id);
	}
	
	public EstadoPlato saveOne(EstadoPlato est) {
		
		Integer estId = this.ep.create(est.getNombreEstadoPlato());
		est.setIdEstadoPlato(estId);
		return est;
	}
	
	public Integer updateOne(EstadoPlato est, Integer id) {
		
		if(est.getIdEstadoPlato() == id) {
			return this.ep.update(id, est.getNombreEstadoPlato());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.ep.del(id);
	}

}
