package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.PagoOrden;
import com.xxi.api.repositories.PagoOrdenDAO;



@Service
public class PagoOrdenService {

	@Autowired
	PagoOrdenDAO p;
	
	public List<PagoOrden> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<PagoOrden> findOne(Integer id){
		
		return this.p.findById(id);
	}
	
	public PagoOrden saveOne(PagoOrden po) {
		
		Integer poId = this.p.create(po.getMontoPagOr(), po.getPropinaPagOr(), po.getOrden().getIdOrden(), po.getMetodoPago().getIdMetodoPago());
		if (poId > 0) {
			po.setIdPagOr(poId);
		}
		return po;
	}
	
	public Integer updateOne(PagoOrden po, Integer id) {

			Integer filas = this.p.update(id, po.getFechaPagOr(), po.getMontoPagOr(), po.getPropinaPagOr(), po.getOrden().getIdOrden(), po.getMetodoPago().getIdMetodoPago());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
