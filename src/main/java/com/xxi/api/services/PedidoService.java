package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Pedido;
import com.xxi.api.repositories.PedidoDAO;

@Service
public class PedidoService {

	@Autowired
	PedidoDAO p;
	
	public List<Pedido> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Pedido> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Pedido saveOne(Pedido pe) {
		
		Integer peId = this.p.create(pe.getTotalPedido(), pe.getUsuario().getIdUsuario());
		pe.setIdPedido(peId);
		return pe;
	}
	
	public Integer updateOne(Pedido pe, Integer id) {

			Integer filas = this.p.update(id, pe.getTotalPedido(), pe.getFechaPedido(), pe.getUsuario().getIdUsuario());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
