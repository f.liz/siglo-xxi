package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Proveedor;
import com.xxi.api.repositories.ProveedorDAO;


@Service
public class ProveedorService {
	
	@Autowired
	ProveedorDAO p;
	
	public List<Proveedor> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Proveedor> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Proveedor saveOne(Proveedor pro) {
		
		Integer proId = this.p.create(pro.getNombreProveedor(), pro.getEmailProveedor(), pro.getTelefonoProveedor(), pro.getEstadoProveedor());
		pro.setIdProveedor(proId);
		return pro;
	}
	
	public Integer updateOne(Proveedor pro, Integer id) {
		
		if(pro.getIdProveedor() == id) {
			return this.p.update(id, pro.getNombreProveedor(), pro.getEmailProveedor(), pro.getTelefonoProveedor(), pro.getEstadoProveedor());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}

}
