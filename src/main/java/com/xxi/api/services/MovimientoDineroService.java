package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.MovimientoDinero;
import com.xxi.api.repositories.MovimientoDineroDAO;



@Service
public class MovimientoDineroService {

	@Autowired
	MovimientoDineroDAO md;
	
	public List<MovimientoDinero> getAll() {
		
		return this.md.findAll();
	}
	
	public Optional<MovimientoDinero> findOne(Integer id) {
		
		return this.md.findById(id);
	}
	
	public MovimientoDinero saveOne(MovimientoDinero mov) {
		
		Integer movId = this.md.create(mov.getMontoMovimiento(), mov.getTipoMovimiento(), mov.getDescripcionMovimiento());
		mov.setIdMovimiento(movId);
		return mov;
	}
	
	public Integer updateOne(MovimientoDinero mov, Integer id) {
		
		if(mov.getIdMovimiento() == id) {
			return this.md.update(id, mov.getMontoMovimiento(), mov.getTipoMovimiento(), mov.getDescripcionMovimiento());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.md.del(id);
	}
}
