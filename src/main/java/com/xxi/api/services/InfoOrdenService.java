package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.InfoOrden;
import com.xxi.api.repositories.InfoOrdenDAO;

@Service
public class InfoOrdenService {

	@Autowired
	InfoOrdenDAO i;
	
	public List<InfoOrden> getAll() {
		
		return this.i.findAll();
	}
	
	public Optional<InfoOrden> findOne(Integer id){
		
		return this.i.findById(id);
	}
	
	public InfoOrden saveOne(InfoOrden io) {
		
		Integer ioId = this.i.create(io.getRegistroHora(), io.getOrden().getIdOrden(), io.getEstadoOrden().getIdEstadoOrden(), io.getUsuario().getIdUsuario());
		if (ioId > 0) {
			io.setIdInfOr(ioId);
		}
		return io;
	}
	
	public Integer updateOne(InfoOrden io, Integer id) {

			Integer filas = this.i.update(id, io.getRegistroHora(), io.getOrden().getIdOrden(), io.getEstadoOrden().getIdEstadoOrden(), io.getUsuario().getIdUsuario());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.i.del(id);
	}
	
}
