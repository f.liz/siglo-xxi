package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Reserva;
import com.xxi.api.repositories.ReservaDAO;


@Service
public class ReservaService {

	@Autowired
	ReservaDAO r;
	
	public List<Reserva> getAll() {
		
		return this.r.findAll();
	}
	
	public Optional<Reserva> findOne(Integer id) {
		
		return this.r.findById(id);
	}
	
	public Reserva saveOne(Reserva re) {
		
		Integer reId = this.r.create(re.getFechaInicio(), re.getFechaTermino(), re.getRutReserva(), re.getMesa().getIdMesa(), re.getUsuario().getIdUsuario());
		re.setIdReserva(reId);
		return re;
	}
	
	public Integer updateOne(Reserva re, Integer id) {

			Integer filas = this.r.update(id, re.getFechaInicio(), re.getFechaTermino(), re.getRutReserva(), re.getMesa().getIdMesa(), re.getUsuario().getIdUsuario());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.r.del(id);
	}
	
}
