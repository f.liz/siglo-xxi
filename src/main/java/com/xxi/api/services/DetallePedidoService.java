package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.DetallePedido;
import com.xxi.api.repositories.DetallePedidoDAO;



@Service
public class DetallePedidoService {

	@Autowired
	DetallePedidoDAO d;
	
	public List<DetallePedido> getAll() {
		
		return this.d.findAll();
	}
	
	public Optional<DetallePedido> findOne(Integer idDetPed){
		
		return this.d.findById(idDetPed);
	}
	
	public DetallePedido saveOne(DetallePedido dp) {
		
		Integer dpId = this.d.create(dp.getCantidadDetPed(), dp.getProvProd().getIdProvProd(), dp.getPedido().getIdPedido());
		if (dpId > 0) {
			dp.setIdDetPed(dpId);
		}
		return dp;
	}
	
	public Integer updateOne(DetallePedido dp, Integer idDetPed) {

			Integer filas = this.d.update(idDetPed, dp.getCantidadDetPed(), dp.getProvProd().getIdProvProd(), dp.getPedido().getIdPedido());
			return filas;
	}
	
	public Integer deleteOne(Integer idDetPed) {
		
		return this.d.del(idDetPed);
	}
	
}
