package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Usuario;
import com.xxi.api.repositories.UsuarioDAO;

@Service
public class UsuarioService {

	@Autowired
	UsuarioDAO u;
	
	public Usuario authenticate(Usuario us) {
		
		return this.u.authenticate(us.getEmailUsuario(), us.getPassUsuario());
	}
	
	public List<Usuario> getAll() {
		
		return this.u.findAll();
	}
	
	public Optional<Usuario> findOne(Integer id) {
		
		return this.u.findById(id);
	}
	
	public Usuario saveOne(Usuario us) {
		
		Integer usId = this.u.create(us.getEmailUsuario(), us.getPassUsuario(), us.getEstadoUsuario(), us.getPerfil().getIdPerfil());
		us.setIdUsuario(usId);
		return us;
	}
	
	public Integer updateOne(Usuario us, Integer id) {

			Integer filas = this.u.update(id, us.getEmailUsuario(), us.getPassUsuario(), us.getEstadoUsuario(), us.getPerfil().getIdPerfil());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.u.del(id);
	}
	
}
