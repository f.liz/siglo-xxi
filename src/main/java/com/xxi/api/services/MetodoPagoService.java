package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.MetodoPago;
import com.xxi.api.repositories.MetodoPagoDAO;


@Service
public class MetodoPagoService {

	@Autowired
	MetodoPagoDAO mp;
	
	public List<MetodoPago> getAll() {
		
		return this.mp.findAll();
	}
	
	public Optional<MetodoPago> findOne(Integer id) {
		
		return this.mp.findById(id);
	}
	
	public MetodoPago saveOne(MetodoPago met) {
		
		Integer metId = this.mp.create(met.getNombreMetodoPago());
		met.setIdMetodoPago(metId);
		return met;
	}
	
	public Integer updateOne(MetodoPago met, Integer id) {
		
		if(met.getIdMetodoPago() == id) {
			return this.mp.update(id, met.getNombreMetodoPago());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.mp.del(id);
	}
}
