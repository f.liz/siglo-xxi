package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Persona;
import com.xxi.api.repositories.PersonaDAO;


@Service
public class PersonaService {

	@Autowired
	PersonaDAO p;
	
	public List<Persona> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<Persona> findOne(Integer id) {
		
		return this.p.findById(id);
	}
	
	public Persona saveOne(Persona pe) {
		
		Integer peId = this.p.create(pe.getRutPerson(), pe.getNombrePerson(), pe.getApellidoPerson(), pe.getUsuario().getIdUsuario());
		pe.setIdPerson(peId);
		return pe;
	}
	
	public Integer updateOne(Persona pe, Integer id) {

			Integer filas = this.p.update(id, pe.getRutPerson(), pe.getNombrePerson(), pe.getApellidoPerson(), pe.getUsuario().getIdUsuario());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.p.del(id);
	}
	
}
