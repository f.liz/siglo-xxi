package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.ProveedorProducto;
import com.xxi.api.repositories.ProveedorProductoDAO;


@Service
public class ProveedorProductoService {

	@Autowired
	ProveedorProductoDAO p;
	
	public List<ProveedorProducto> getAll() {
		
		return this.p.findAll();
	}
	
	public Optional<ProveedorProducto> findOne(Integer idProvProd){
		
		return this.p.findById(idProvProd);
	}
	
	public ProveedorProducto saveOne(ProveedorProducto pp) {
		
		Integer ppId = this.p.create(pp.getValorProducto(), pp.getProveedor().getIdProveedor(), pp.getProducto().getIdProducto());
		if (ppId > 0) {
			pp.setIdProvProd(ppId);
		}
		return pp;
	}
	
	public Integer updateOne(ProveedorProducto pp, Integer idProvProd) {

			Integer filas = this.p.update(idProvProd, pp.getValorProducto(), pp.getProveedor().getIdProveedor(), pp.getProducto().getIdProducto());
			return filas;
	}
	
	public Integer deleteOne(Integer idProvProd) {
		
		return this.p.del(idProvProd);
	}
	
}
