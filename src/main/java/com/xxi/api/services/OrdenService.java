package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.Orden;
import com.xxi.api.repositories.OrdenDAO;



@Service
public class OrdenService {

	@Autowired
	OrdenDAO o;
	
	public List<Orden> getAll() {
		
		return this.o.findAll();
	}
	
	public Optional<Orden> findOne(Integer id) {
		
		return this.o.findById(id);
	}
	
	public Orden saveOne(Orden or) {
		
		Integer orId = this.o.create(or.getFechaEnt(), or.getTotalOrden(), or.getEstadoOrden(), or.getTipo().getIdTipo(), or.getMesa().getIdMesa());
		or.setIdOrden(orId);
		return or;
	}
	
	public Integer updateOne(Orden or, Integer id) {

			Integer filas = this.o.update(id, or.getFechaEnt(), or.getTotalOrden(), or.getEstadoOrden(), or.getTipo().getIdTipo(), or.getMesa().getIdMesa());
			return filas;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.o.del(id);
	}
	
}
