package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.EstadoMesa;
import com.xxi.api.repositories.EstadoMesaDAO;

@Service
public class EstadoMesaService {
	
	@Autowired
	EstadoMesaDAO em;
	
	public List<EstadoMesa> getAll() {
		
		return this.em.findAll();
	}
	
	public Optional<EstadoMesa> findOne(Integer id) {
		
		return this.em.findById(id);
	}
	
	public EstadoMesa saveOne(EstadoMesa est) {
		
		Integer estId = this.em.create(est.getNombreEstadoMesa());
		est.setIdEstadoMesa(estId);
		return est;
	}
	
	public Integer updateOne(EstadoMesa est, Integer id) {
		
		if(est.getIdEstadoMesa() == id) {
			return this.em.update(id, est.getNombreEstadoMesa());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.em.del(id);
	}

}
