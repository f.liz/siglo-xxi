package com.xxi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xxi.api.entities.UnidadMedida;
import com.xxi.api.repositories.UnidadMedidaDAO;

@Service
public class UnidadMedidaService {
	@Autowired
	UnidadMedidaDAO u;
	
	public List<UnidadMedida> getAll() {
		
		return this.u.findAll();
	}
	
	public Optional<UnidadMedida> findOne(Integer id) {
		
		return this.u.findById(id);
	}
	
	public UnidadMedida saveOne(UnidadMedida uni) {
		
		Integer uniId = this.u.create(uni.getCodigoUnidadMedida(), uni.getNombreUnidadMedida());
		uni.setIdUnidadMedida(uniId);
		return uni;
	}
	
	public Integer updateOne(UnidadMedida uni, Integer id) {
		
		if(uni.getIdUnidadMedida() == id) {
			return this.u.update(id, uni.getCodigoUnidadMedida(), uni.getNombreUnidadMedida());
		}
		return 0;
	}
	
	public Integer deleteOne(Integer id) {
		
		return this.u.del(id);
	}
}
