package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.EstadoMesa;

@Repository
public interface EstadoMesaDAO extends JpaRepository<EstadoMesa, Integer>{
	
	@Procedure(name = "listEstadosMesa")
    List<EstadoMesa> getMesas();

	@Transactional
	@Procedure(name="createEstadoMesa")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateEstadoMesa")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="deleteEstadoMesa")
	Integer del(@Param("v_id") Integer v_id);
}
