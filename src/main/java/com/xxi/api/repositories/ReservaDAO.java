package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Reserva;

@Repository
public interface ReservaDAO extends JpaRepository<Reserva, Integer>{

	@Procedure(name = "listReservas")
    List<Reserva> getReservas();
	
	@Transactional
	@Procedure(name="createReserva")
	Integer create(@Param("v_fechaini") Date v_fechaini, @Param("v_fechater") Date v_fechater, @Param("v_rut") String v_rut, @Param("v_idmesa") Integer v_idmesa, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="updateReserva")
	Integer update(@Param("v_idreserva") Integer v_idreserva, @Param("v_fechaini") Date v_fechaini, @Param("v_fechater") Date v_fechater, @Param("v_rut") String v_rut, @Param("v_idmesa") Integer v_idmesa, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="deleteReserva")
	Integer del(@Param("v_idreserva") Integer v_idreserva);
}
