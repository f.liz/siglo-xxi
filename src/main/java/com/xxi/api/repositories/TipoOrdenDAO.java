package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.TipoOrden;

@Repository
public interface TipoOrdenDAO extends JpaRepository<TipoOrden, Integer>{
	
	@Procedure(name = "listTipos")
    List<TipoOrden> getTipos();

	@Transactional
	@Procedure(name="createTipoOrden")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateTipoOrden")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="delTipoOrden")
	Integer del(@Param("v_id") Integer v_id);
}
