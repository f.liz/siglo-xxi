package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.PagoPedido;


@Repository
public interface PagoPedidoDAO extends JpaRepository<PagoPedido, Integer>{

	@Procedure(name = "listPagoPedidos")
    List<PagoPedido> getPagoPedidos();
	
	@Transactional
	@Procedure(name="createPagoPedido")
	Integer create(@Param("v_fecha") Date v_fecha, @Param("v_monto") Integer v_monto, @Param("v_descripcion") String v_descripcion, @Param("v_idpedido") Integer v_idpedido);
	
	@Transactional
	@Procedure(name="updatePagoPedido")
	Integer update(@Param("v_idpago") Integer v_idpago, @Param("v_fecha") Date v_fecha, @Param("v_monto") Integer v_monto, @Param("v_descripcion") String v_descripcion, @Param("v_idpedido") Integer v_idpedido);
	
	@Transactional
	@Procedure(name="deletePagoPedido")
	Integer del(@Param("v_idpago") Integer v_idpago);
}
