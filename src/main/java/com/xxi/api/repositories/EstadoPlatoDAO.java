package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.EstadoPlato;

@Repository
public interface EstadoPlatoDAO extends JpaRepository<EstadoPlato, Integer>{
	
	@Procedure(name = "listEstadosPlato")
    List<EstadoPlato> getPlatos();

	@Transactional
	@Procedure(name="createEstadoPlato")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateEstadoPlato")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="deleteEstadoPlato")
	Integer del(@Param("v_id") Integer v_id);
}
