package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Receta;



@Repository
public interface RecetaDAO extends JpaRepository<Receta, Integer>{

	@Procedure(name = "listRecetas")
    List<Receta> getDetallePedido();
	
	@Transactional
	@Procedure(name="createReceta")
	Integer create(@Param("v_cantidad") Double v_cantidad, @Param("v_idproducto") Integer v_idproducto, @Param("v_idplato") Integer v_idplato);
	
	@Transactional
	@Procedure(name="updateReceta")
	Integer update(@Param("v_idreceta") Integer v_idreceta, @Param("v_cantidad") Double v_cantidad, @Param("v_idproducto") Integer v_idproducto, @Param("v_idplato") Integer v_idplato);
	
	@Transactional
	@Procedure(name="deleteReceta")
	Integer del(@Param("v_idreceta") Integer v_idreceta);
}
