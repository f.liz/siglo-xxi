package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Persona;


@Repository
public interface PersonaDAO extends JpaRepository<Persona, Integer>{

	@Procedure(name = "listUsuarios")
    List<Persona> getPersonas();
	
	@Transactional
	@Procedure(name="createPersona")
	Integer create(@Param("v_rut") String v_rut, @Param("v_nombre") String v_nombre, @Param("v_apellido") String v_apellido, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="updatePersona")
	Integer update(@Param("v_idpersona") Integer v_idpersona, @Param("v_rut") String v_rut, @Param("v_nombre") String v_nombre, @Param("v_apellido") String v_apellido, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="deletePersona")
	Integer del(@Param("v_idpersona") Integer v_idpersona);
}
