package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Plato;


@Repository
public interface PlatoDAO extends JpaRepository<Plato, Integer>{

	@Procedure(name = "listPlatos")
    List<Plato> getPlato();
	
	@Transactional
	@Procedure(name="createPlato")
	Integer create(@Param("v_nombre") String v_nombre, @Param("v_valor") Integer v_valor, @Param("v_tiempo") Integer v_tiempo, @Param("v_preparacion") String v_preparacion, @Param("v_idcategoria") Integer v_idcategoria);
	
	@Transactional
	@Procedure(name="updatePlato")
	Integer update(@Param("v_idplato") Integer v_idplato, @Param("v_nombre") String v_nombre, @Param("v_valor") Integer v_valor, @Param("v_tiempo") Integer v_tiempo, @Param("v_preparacion") String v_preparacion, @Param("v_idcategoria") Integer v_idcategoria);
	
	@Transactional
	@Procedure(name="deletePlato")
	Integer del(@Param("v_idplato") Integer v_idplato);
}
