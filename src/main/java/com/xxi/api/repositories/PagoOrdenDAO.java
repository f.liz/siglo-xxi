package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.PagoOrden;


@Repository
public interface PagoOrdenDAO extends JpaRepository<PagoOrden, Integer>{

	@Procedure(name = "listPagoOrdenes")
    List<PagoOrden> getPagoOrdenes();
	
	@Transactional
	@Procedure(name="createPagoOrden")
	Integer create(@Param("v_monto") Integer v_monto, @Param("v_propina") Integer v_propina, @Param("v_idorden") Integer v_idorden, @Param("v_idmetodo") Integer v_idmetodo);
	
	@Transactional
	@Procedure(name="updatePagoOrden")
	Integer update(@Param("v_idpago") Integer v_idpago, @Param("v_fecha") Date v_fecha, @Param("v_monto") Integer v_monto, @Param("v_propina") Integer v_propina, @Param("v_idorden") Integer v_idorden, @Param("v_idmetodo") Integer v_idmetodo);
	
	@Transactional
	@Procedure(name="deletePagoOrden")
	Integer del(@Param("v_idpago") Integer v_idpago);
}
