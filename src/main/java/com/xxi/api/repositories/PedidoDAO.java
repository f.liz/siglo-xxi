package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Pedido;


@Repository
public interface PedidoDAO extends JpaRepository<Pedido, Integer>{

	@Procedure(name = "listPedidos")
    List<Pedido> getPedidos();
	
	@Transactional
	@Procedure(name="createPedido")
	Integer create(@Param("v_total") Integer v_total, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="updatePedido")
	Integer update(@Param("v_idpedido") Integer v_idpedido, @Param("v_total") Integer v_total, @Param("v_fecha") Date v_fecha, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="deletePedido")
	Integer del(@Param("v_idpedido") Integer v_idpedido);
}
