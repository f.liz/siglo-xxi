package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.EstadoOrden;

@Repository
public interface EstadoOrdenDAO extends JpaRepository<EstadoOrden, Integer>{
	
	@Procedure(name = "listEstadosOrden")
    List<EstadoOrden> getOrdenes();

	@Transactional
	@Procedure(name="createEstadoOrden")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateEstadoOrden")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="deleteEstadoOrden")
	Integer del(@Param("v_id") Integer v_id);
}
