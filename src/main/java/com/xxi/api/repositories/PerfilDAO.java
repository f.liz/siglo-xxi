package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Perfil;

@Repository
public interface PerfilDAO extends JpaRepository<Perfil, Integer> {
	
	@Procedure(name = "listPerfiles")
    List<Perfil> getPerfiles();

	@Transactional
	@Procedure(name="createPerfil")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updatePerfil")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="delPerfil")
	Integer del(@Param("v_id") Integer v_id);
}
