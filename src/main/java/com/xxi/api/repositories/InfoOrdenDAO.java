package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.InfoOrden;

@Repository
public interface InfoOrdenDAO extends JpaRepository<InfoOrden, Integer>{

	@Procedure(name = "listInfoOrdenes")
    List<InfoOrden> getDetallePedido();
	
	@Transactional
	@Procedure(name="createInfoOrden")
	Integer create(@Param("v_hora") Date v_hora, @Param("v_idorden") Integer v_idorden, @Param("v_idestado") Integer v_idestado, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="updateInfoOrden")
	Integer update(@Param("v_idinfor") Integer v_idinfor, @Param("v_hora") Date v_hora, @Param("v_idorden") Integer v_idorden, @Param("v_idestado") Integer v_idestado, @Param("v_idusuario") Integer v_idusuario);
	
	@Transactional
	@Procedure(name="deleteInfoOrden")
	Integer del(@Param("v_idinfor") Integer v_idinfor);
}
