package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Proveedor;

@Repository
public interface ProveedorDAO extends JpaRepository<Proveedor, Integer> {
	
	@Procedure(name = "listProveedores")
    List<Proveedor> getProveedores();

	@Transactional
	@Procedure(name="createProveedor")
	Integer create(@Param("v_nombre") String v_nombre, @Param("v_email") String v_email, @Param("v_telefono") String v_telefono, @Param("v_estado") String v_estado);
	
	@Transactional
	@Procedure(name="updateProveedor")
	Integer update(@Param("v_idprov") Integer v_idprov, @Param("v_nombre") String v_nombre, @Param("v_email") String v_email, @Param("v_telefono") String v_telefono, @Param("v_estado") String v_estado);
	
	@Transactional
	@Procedure(name="delProveedor")
	Integer del(@Param("v_idprov") Integer v_idprov);
}