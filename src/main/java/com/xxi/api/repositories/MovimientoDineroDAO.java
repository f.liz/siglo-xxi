package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.MovimientoDinero;

@Repository
public interface MovimientoDineroDAO extends JpaRepository<MovimientoDinero, Integer> {

	@Procedure(name = "listMovimientos")
    List<MovimientoDinero> getMovimientos();
	
	@Transactional
	@Procedure(name="createMovimiento")
	Integer create(@Param("v_monto") Integer v_monto, @Param("v_tipo") String v_tipo, @Param("v_desc") String v_desc);
	
	@Transactional
	@Procedure(name="updateMovimiento")
	Integer update(@Param("v_id") Integer v_id, @Param("v_monto") Integer v_monto, @Param("v_tipo") String v_tipo, @Param("v_desc") String v_desc);
	
	@Transactional
	@Procedure(name="delMovimiento")
	Integer del(@Param("v_id") Integer v_id);
}
