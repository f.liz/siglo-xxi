package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Categoria;

@Repository
public interface CategoriaDAO extends JpaRepository<Categoria, Integer> {
	
	@Procedure(name = "listCategorias")
    List<Categoria> getCategorias();

	@Transactional
	@Procedure(name="createCategoria")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateCategoria")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="delCategoria")
	Integer del(@Param("v_id") Integer v_id);
}