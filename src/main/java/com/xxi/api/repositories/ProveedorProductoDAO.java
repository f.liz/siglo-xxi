package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.ProveedorProducto;

@Repository
public interface ProveedorProductoDAO extends JpaRepository<ProveedorProducto, Integer>{

	@Procedure(name = "listProveedorProductos")
    List<ProveedorProducto> getProveedorPedidos();
	
	@Transactional
	@Procedure(name="createProveedorProducto")
	Integer create(@Param("v_valor") Integer v_valor, @Param("v_idproveedor") Integer v_idproveedor, @Param("v_idproducto") Integer v_idproducto);
	
	@Transactional
	@Procedure(name="updateProveedorProducto")
	Integer update(@Param("v_idprovprod") Integer v_idprovprod, @Param("v_valor") Integer v_valor, @Param("v_idproveedor") Integer v_idproveedor, @Param("v_idproducto") Integer v_idproducto);
	
	@Transactional
	@Procedure(name="deleteProveedorProducto")
	Integer del(@Param("v_idprovprod") Integer v_idprovprod);
}
