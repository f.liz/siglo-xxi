package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Orden;


@Repository
public interface OrdenDAO extends JpaRepository<Orden, Integer>{

	@Procedure(name = "listOrdenes")
    List<Orden> getReservas();
	
	@Transactional
	@Procedure(name="createOrden")
	Integer create(@Param("v_fechaentrega") Date v_fechaentrega, @Param("v_total") Integer v_total, @Param("v_estado") String v_estado, @Param("v_idtipo") Integer v_idtipo, @Param("v_idmesa") Integer v_idmesa);
	
	@Transactional
	@Procedure(name="updateOrden")
	Integer update(@Param("v_idorden") Integer v_idorden, @Param("v_fechaentrega") Date v_fechaentrega, @Param("v_total") Integer v_total, @Param("v_estado") String v_estado, @Param("v_idtipo") Integer v_idtipo, @Param("v_idmesa") Integer v_idmesa);
	
	@Transactional
	@Procedure(name="deleteOrden")
	Integer del(@Param("v_idorden") Integer v_idorden);
}
