package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.DetalleOrden;



@Repository
public interface DetalleOrdenDAO extends JpaRepository<DetalleOrden, Integer>{

	@Procedure(name = "listDetalleOrdenes")
    List<DetalleOrden> getDetallePedido();
	
	@Transactional
	@Procedure(name="createDetalleOrden")
	Integer create(@Param("v_cantidad") Integer v_cantidad, @Param("v_idplato") Integer v_idplato, @Param("v_estado") Integer v_estado, @Param("v_orden") Integer v_orden);
	
	@Transactional
	@Procedure(name="updateDetalleOrden")
	Integer update(@Param("v_iddetor") Integer v_iddetor, @Param("v_cantidad") Integer v_cantidad, @Param("v_idplato") Integer v_idplato, @Param("v_estado") Integer v_estado, @Param("v_orden") Integer v_orden);
	
	@Transactional
	@Procedure(name="deleteDetalleOrden")
	Integer del(@Param("v_iddetor") Integer v_iddetor);
}
