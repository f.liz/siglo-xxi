package com.xxi.api.repositories;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Mesa;

@Repository
public interface MesaDAO extends JpaRepository<Mesa, Integer>{

    @Query(value = "SELECT * FROM MESA M JOIN RESERVA R ON (M.IDMESA = R.IDMESA) WHERE M.CAPACIDADMESA >= :capacidad AND R.FECHAINIRES NOT BETWEEN :fecha - 2/24 AND :fecha + 2/24 ORDER BY M.CAPACIDADMESA", nativeQuery = true) 
    List<Mesa> getMesasReserva(@Param("capacidad") Integer capacidad, @Param("fecha") Date fecha);
	
	@Procedure(name = "listMesas")
    List<Mesa> getMesas();
	
	@Transactional
	@Procedure(name="createMesa")
	Integer create(@Param("v_numero") Integer v_numero, @Param("v_capacidad") Integer v_capacidad, @Param("v_estado") Integer v_estado);
	
	@Transactional
	@Procedure(name="updateMesa")
	Integer update(@Param("v_id") Integer v_id, @Param("v_numero") Integer v_numero, @Param("v_capacidad") Integer v_capacidad, @Param("v_estado") Integer v_estado);
	
	@Transactional
	@Procedure(name="deleteMesa")
	Integer del(@Param("v_id") Integer v_id);
}
