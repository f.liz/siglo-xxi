package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Usuario;

@Repository
public interface UsuarioDAO extends JpaRepository<Usuario, Integer>{
	
    @Query(value = "SELECT * FROM USUARIO WHERE EMAILUSUARIO = :email AND PASSUSUARIO = :pass", nativeQuery = true) 
    Usuario authenticate(@Param("email") String email, @Param("pass") String pass);

	@Procedure(name = "listUsuarios")
    List<Usuario> getUsuarios();
	
	@Transactional
	@Procedure(name="createUsuario")
	Integer create(@Param("v_email") String v_email, @Param("v_pass") String v_pass, @Param("v_estado") String v_estado, @Param("v_perfil") Integer v_perfil);
	
	@Transactional
	@Procedure(name="updateUsuario")
	Integer update(@Param("v_id") Integer v_id, @Param("v_email") String v_email, @Param("v_pass") String v_pass, @Param("v_estado") String v_estado, @Param("v_perfil") Integer v_perfil);
	
	@Transactional
	@Procedure(name="deleteUsuario")
	Integer del(@Param("v_id") Integer v_id);
}
