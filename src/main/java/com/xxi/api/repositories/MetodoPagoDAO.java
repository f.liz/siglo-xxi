package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.MetodoPago;

@Repository
public interface MetodoPagoDAO extends JpaRepository<MetodoPago, Integer>{
	
	@Procedure(name = "listMetodos")
    List<MetodoPago> getMetods();
	
	@Transactional
	@Procedure(name="createMetodo")
	Integer create(@Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateMetodo")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="delMetodo")
	Integer del(@Param("v_id") Integer v_id);
}
