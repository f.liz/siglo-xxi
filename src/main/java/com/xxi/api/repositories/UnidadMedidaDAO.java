package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.xxi.api.entities.UnidadMedida;

public interface UnidadMedidaDAO extends JpaRepository<UnidadMedida, Integer>{

	@Procedure(name = "listUnidades")
    List<UnidadMedida> getUnidades();
	
	@Transactional
	@Procedure(name="createUnidad")
	Integer create(@Param("v_cod") String v_cod, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="updateUnidad")
	Integer update(@Param("v_id") Integer v_id, @Param("v_cod") String v_cod, @Param("v_nombre") String v_nombre);
	
	@Transactional
	@Procedure(name="delUnidad")
	Integer del(@Param("v_id") Integer v_id);
}
