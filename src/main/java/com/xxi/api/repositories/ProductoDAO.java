package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.Producto;


@Repository
public interface ProductoDAO extends JpaRepository<Producto, Integer>{

	@Procedure(name = "listProductos")
    List<Producto> getUsuarios();
	
	@Transactional
	@Procedure(name="createProducto")
	Integer create(@Param("v_nombre") String v_nombre, @Param("v_stock") Double v_stock, @Param("v_min") Double v_min, @Param("v_unidad") Integer v_unidad);
	
	@Transactional
	@Procedure(name="updateProducto")
	Integer update(@Param("v_id") Integer v_id, @Param("v_nombre") String v_nombre, @Param("v_stock") Double v_stock, @Param("v_min") Double v_min, @Param("v_unidad") Integer v_unidad);
	
	@Transactional
	@Procedure(name="deleteProducto")
	Integer del(@Param("v_id") Integer v_id);
}
