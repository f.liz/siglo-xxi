package com.xxi.api.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xxi.api.entities.DetallePedido;


@Repository
public interface DetallePedidoDAO extends JpaRepository<DetallePedido, Integer>{

	@Procedure(name = "listDetallePedidos")
    List<DetallePedido> getDetallePedido();
	
	@Transactional
	@Procedure(name="createDetallePedido")
	Integer create(@Param("v_cantidad") Double v_cantidad, @Param("v_idprovprod") Integer v_idprovprod, @Param("v_idpedido") Integer v_idpedido);
	
	@Transactional
	@Procedure(name="updateDetallePedido")
	Integer update(@Param("v_iddetped") Integer v_iddetped, @Param("v_cantidad") Double v_cantidad, @Param("v_idprovprod") Integer v_idprovprod, @Param("v_idpedido") Integer v_idpedido);
	
	@Transactional
	@Procedure(name="deleteDetallePedido")
	Integer del(@Param("v_iddetped") Integer v_iddetped);
}
