package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Pedido;
import com.xxi.api.services.PedidoService;


@RestController
public class PedidoController {

	
	@Autowired
	PedidoService service;
	
	@GetMapping("/api/pedidos")
	public ResponseEntity<List<Pedido>> getAll(){
		
		List<Pedido> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/pedidos/{id}")
	public ResponseEntity<Optional<Pedido>> findOne(@PathVariable Integer id){
		
		Optional<Pedido> pedido = service.findOne(id);
		if(pedido != null) {
			return new ResponseEntity<>(pedido, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/pedidos")
	public ResponseEntity<Pedido> saveOne(@RequestBody Pedido pe){
		try {
			Pedido pedido = service.saveOne(pe);
			if(pedido.getIdPedido() > 0){
				return new ResponseEntity<>(pedido, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/pedidos/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Pedido pe, @PathVariable Integer id){
		
		try {
			if(pe.getIdPedido() == id) {
				Integer filas = service.updateOne(pe, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/pedidos/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
