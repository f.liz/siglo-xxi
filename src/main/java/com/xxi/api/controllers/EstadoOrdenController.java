package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.EstadoOrden;
import com.xxi.api.services.EstadoOrdenService;

@RestController
public class EstadoOrdenController {

	@Autowired
	EstadoOrdenService service;
	
	@GetMapping("/api/estadosorden")
	public ResponseEntity<List<EstadoOrden>> getAll(){
		
		List<EstadoOrden> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/estadosorden/{id}")
	public ResponseEntity<Optional<EstadoOrden>> findOne(@PathVariable Integer id){
		
		Optional<EstadoOrden> orden = service.findOne(id);
		if(orden != null) {
			return new ResponseEntity<>(orden, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/estadosorden")
	public ResponseEntity<EstadoOrden> saveOne(@RequestBody EstadoOrden est){
		
		try {
			EstadoOrden eo = service.saveOne(est);
			if(eo.getIdEstadoOrden() > 0){
				return new ResponseEntity<>(eo, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/estadosorden/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody EstadoOrden est, @PathVariable Integer id){
		
		try {
			if(est.getIdEstadoOrden() == id) {
				Integer filas = service.updateOne(est, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/estadosorden/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
