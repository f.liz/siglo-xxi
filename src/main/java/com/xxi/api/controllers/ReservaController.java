package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Reserva;
import com.xxi.api.services.ReservaService;




@RestController
public class ReservaController {

	
	@Autowired
	ReservaService service;
	
	@GetMapping("/api/reservas")
	public ResponseEntity<List<Reserva>> getAll(){
		
		List<Reserva> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/reservas/{id}")
	public ResponseEntity<Optional<Reserva>> findOne(@PathVariable Integer id){
		
		Optional<Reserva> reserva = service.findOne(id);
		if(reserva != null) {
			return new ResponseEntity<>(reserva, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/reservas")
	public ResponseEntity<Reserva> saveOne(@RequestBody Reserva us){
		try {
			Reserva reserva = service.saveOne(us);
			if(reserva.getIdReserva() > 0){
				return new ResponseEntity<>(reserva, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/reservas/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Reserva us, @PathVariable Integer id){
		
		try {
			if(us.getIdReserva() == id) {
				Integer filas = service.updateOne(us, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/reservas/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
