package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.ProveedorProducto;
import com.xxi.api.services.ProveedorProductoService;


@RestController
public class ProveedorProductoController {

	
	@Autowired
	ProveedorProductoService service;
	
	@GetMapping("/api/proveedorproductos")
	public ResponseEntity<List<ProveedorProducto>> getAll(){
		
		List<ProveedorProducto> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/proveedorproductos/{id}")
	public ResponseEntity<Optional<ProveedorProducto>> findOne(@PathVariable Integer id){
		
		Optional<ProveedorProducto> proveedorproducto = service.findOne(id);
		if(proveedorproducto != null) {
			return new ResponseEntity<>(proveedorproducto, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/proveedorproductos")
	public ResponseEntity<ProveedorProducto> saveOne(@RequestBody ProveedorProducto pp){
		try {
			ProveedorProducto proveedorproducto = service.saveOne(pp);
			if(proveedorproducto.getIdProvProd() > 0){
				return new ResponseEntity<>(proveedorproducto, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/proveedorproductos/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody ProveedorProducto pp, @PathVariable Integer id){
		
		try {
			if(pp.getIdProvProd() == id) {
				Integer filas = service.updateOne(pp, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/proveedorproductos/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
