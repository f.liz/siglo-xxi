package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.PagoOrden;
import com.xxi.api.services.PagoOrdenService;




@RestController
public class PagoOrdenController {

	
	@Autowired
	PagoOrdenService service;
	
	@GetMapping("/api/pagoordenes")
	public ResponseEntity<List<PagoOrden>> getAll(){
		
		List<PagoOrden> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/pagoordenes/{id}")
	public ResponseEntity<Optional<PagoOrden>> findOne(@PathVariable Integer id){
		
		Optional<PagoOrden> pagoorden = service.findOne(id);
		if(pagoorden != null) {
			return new ResponseEntity<>(pagoorden, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/pagoordenes")
	public ResponseEntity<PagoOrden> saveOne(@RequestBody PagoOrden po){
		try {
			PagoOrden pagoorden = service.saveOne(po);
			if(pagoorden.getIdPagOr() > 0){
				return new ResponseEntity<>(pagoorden, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/pagoordenes/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody PagoOrden po, @PathVariable Integer id){
		
		try {
			if(po.getIdPagOr() == id) {
				Integer filas = service.updateOne(po, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/pagoordenes/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
