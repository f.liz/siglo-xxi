package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.EstadoMesa;
import com.xxi.api.services.EstadoMesaService;

@RestController
public class EstadoMesaController {

	@Autowired
	EstadoMesaService service;
	
	@GetMapping("/api/estadosmesa")
	public ResponseEntity<List<EstadoMesa>> getAll(){
		
		List<EstadoMesa> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/estadosmesa/{id}")
	public ResponseEntity<Optional<EstadoMesa>> findOne(@PathVariable Integer id){
		
		Optional<EstadoMesa> mesa = service.findOne(id);
		if(mesa != null) {
			return new ResponseEntity<>(mesa, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/estadosmesa")
	public ResponseEntity<EstadoMesa> saveOne(@RequestBody EstadoMesa est){
		
		try {
			EstadoMesa em = service.saveOne(est);
			if(em.getIdEstadoMesa() > 0){
				return new ResponseEntity<>(em, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/estadosmesa/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody EstadoMesa est, @PathVariable Integer id){
		
		try {
			if(est.getIdEstadoMesa() == id) {
				Integer filas = service.updateOne(est, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/estadosmesa/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
