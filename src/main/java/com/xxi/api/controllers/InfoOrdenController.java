package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.InfoOrden;
import com.xxi.api.services.InfoOrdenService;

@RestController
public class InfoOrdenController {

	
	@Autowired
	InfoOrdenService service;
	
	@GetMapping("/api/infoordenes")
	public ResponseEntity<List<InfoOrden>> getAll(){
		
		List<InfoOrden> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/infoordenes/{id}")
	public ResponseEntity<Optional<InfoOrden>> findOne(@PathVariable Integer id){
		
		Optional<InfoOrden> infoorden = service.findOne(id);
		if(infoorden != null) {
			return new ResponseEntity<>(infoorden, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/infoordenes")
	public ResponseEntity<InfoOrden> saveOne(@RequestBody InfoOrden io){
		try {
			InfoOrden infoorden = service.saveOne(io);
			if(infoorden.getIdInfOr() > 0){
				return new ResponseEntity<>(infoorden, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/infoordenes/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody InfoOrden io, @PathVariable Integer id){
		
		try {
			if(io.getIdInfOr() == id) {
				Integer filas = service.updateOne(io, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/infoordenes/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
