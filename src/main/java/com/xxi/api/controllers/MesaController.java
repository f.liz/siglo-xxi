package com.xxi.api.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Mesa;
import com.xxi.api.services.MesaService;

@RestController
public class MesaController {

	
	@Autowired
	MesaService service;
	
	@GetMapping("/api/mesas")
	public ResponseEntity<List<Mesa>> getAll(){
		
		List<Mesa> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/mesas/{id}")
	public ResponseEntity<Optional<Mesa>> findOne(@PathVariable Integer id){
		
		Optional<Mesa> mesa = service.findOne(id);
		if(mesa != null) {
			return new ResponseEntity<>(mesa, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/mesas/reserva/{cap}/{fecha}")
	public ResponseEntity<List<Mesa>> getMesasReserva(@PathVariable Integer cap, @PathVariable @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm") Date fecha){
		//String pattern = "dd-MM-yyyy HH:mm";
		//SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		//String date = simpleDateFormat.format(fecha);
		//return date;
		List<Mesa> lista = service.getMesasReserva(cap, fecha);
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/mesas")
	public ResponseEntity<Mesa> saveOne(@RequestBody Mesa me){
		try {
			Mesa mesa = service.saveOne(me);
			if(mesa.getIdMesa() > 0){
				return new ResponseEntity<>(mesa, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/mesas/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Mesa me, @PathVariable Integer id){
		
		try {
			if(me.getIdMesa() == id) {
				Integer filas = service.updateOne(me, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/mesas/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
