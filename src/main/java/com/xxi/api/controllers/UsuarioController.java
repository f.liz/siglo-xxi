package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.xxi.api.entities.Usuario;
import com.xxi.api.services.UsuarioService;

@RestController
public class UsuarioController {

	
	@Autowired
	UsuarioService service;
	
	@GetMapping("/api/usuarios")
	public ResponseEntity<List<Usuario>> getAll(){
		
		List<Usuario> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/usuarios/{id}")
	public ResponseEntity<Optional<Usuario>> findOne(@PathVariable Integer id){
		
		Optional<Usuario> usuario = service.findOne(id);
		if(usuario != null) {
			return new ResponseEntity<>(usuario, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/usuarios")
	public ResponseEntity<Usuario> saveOne(@RequestBody Usuario us){
		try {
			Usuario usuario = service.saveOne(us);
			if(usuario.getIdUsuario() > 0){
				return new ResponseEntity<>(usuario, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PostMapping("/api/usuarios/authenticate")
	public ResponseEntity<Usuario> authenticate(@RequestBody Usuario us){
		try {
			Usuario usuario = service.authenticate(us);
			if(usuario != null && usuario.getIdUsuario() > 0){
				return new ResponseEntity<>(usuario, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/usuarios/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Usuario us, @PathVariable Integer id){
		
		try {
			if(us.getIdUsuario() == id) {
				Integer filas = service.updateOne(us, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/usuarios/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
