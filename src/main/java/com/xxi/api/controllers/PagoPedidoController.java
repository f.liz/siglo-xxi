package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.PagoPedido;
import com.xxi.api.services.PagoPedidoService;


@RestController
public class PagoPedidoController {

	
	@Autowired
	PagoPedidoService service;
	
	@GetMapping("/api/pagopedidos")
	public ResponseEntity<List<PagoPedido>> getAll(){
		
		List<PagoPedido> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/pagoedidos/{id}")
	public ResponseEntity<Optional<PagoPedido>> findOne(@PathVariable Integer id){
		
		Optional<PagoPedido> pagopedido = service.findOne(id);
		if(pagopedido != null) {
			return new ResponseEntity<>(pagopedido, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/pagopedidos")
	public ResponseEntity<PagoPedido> saveOne(@RequestBody PagoPedido pp){
		try {
			PagoPedido pagopedido = service.saveOne(pp);
			if(pagopedido.getIdPagoPedido() > 0){
				return new ResponseEntity<>(pagopedido, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/pagopedidos/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody PagoPedido pp, @PathVariable Integer id){
		
		try {
			if(pp.getIdPagoPedido() == id) {
				Integer filas = service.updateOne(pp, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/pagopedidos/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
