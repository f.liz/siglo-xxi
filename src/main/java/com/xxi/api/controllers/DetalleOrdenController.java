package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.DetalleOrden;
import com.xxi.api.services.DetalleOrdenService;



@RestController
public class DetalleOrdenController {

	
	@Autowired
	DetalleOrdenService service;
	
	@GetMapping("/api/detalleordenes")
	public ResponseEntity<List<DetalleOrden>> getAll(){
		
		List<DetalleOrden> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/detalleordenes/{id}")
	public ResponseEntity<Optional<DetalleOrden>> findOne(@PathVariable Integer id){
		
		Optional<DetalleOrden> detalleorden = service.findOne(id);
		if(detalleorden != null) {
			return new ResponseEntity<>(detalleorden, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/detalleordenes")
	public ResponseEntity<DetalleOrden> saveOne(@RequestBody DetalleOrden det){
		try {
			DetalleOrden detalleorden = service.saveOne(det);
			if(detalleorden.getIdDetOr() > 0){
				return new ResponseEntity<>(detalleorden, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/detalleordenes/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody DetalleOrden det, @PathVariable Integer id){
		
		try {
			if(det.getIdDetOr() == id) {
				Integer filas = service.updateOne(det, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/detalleordenes/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
