package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Categoria;
import com.xxi.api.services.CategoriaService;

@RestController
public class CategoriaController {

	@Autowired
	CategoriaService service;
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/api/categorias")
	public ResponseEntity<List<Categoria>> getAll(){
		
		List<Categoria> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/api/categorias/{id}")
	public ResponseEntity<Optional<Categoria>> findOne(@PathVariable Integer id){
		
		Optional<Categoria> categoria = service.findOne(id);
		if(categoria != null) {
			return new ResponseEntity<>(categoria, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/api/categorias")
	public ResponseEntity<Categoria> saveOne(@RequestBody Categoria cat){
		
		try {
			Categoria categoria = service.saveOne(cat);
			if(categoria.getIdCategoria() > 0){
				return new ResponseEntity<>(categoria, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@CrossOrigin(origins = "http://localhost:3000")
	@PutMapping("/api/categorias/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Categoria cat, @PathVariable Integer id){
		
		try {
			if(cat.getIdCategoria() == id) {
				Integer filas = service.updateOne(cat, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@CrossOrigin(origins = "http://localhost:3000")
	@DeleteMapping("/api/categorias/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
