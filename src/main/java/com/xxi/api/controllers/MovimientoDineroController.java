package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.MovimientoDinero;
import com.xxi.api.services.MovimientoDineroService;



@RestController
public class MovimientoDineroController {

	@Autowired
	MovimientoDineroService service;
	
	@GetMapping("/api/movimientos")
	public ResponseEntity<List<MovimientoDinero>> getAll(){
		
		List<MovimientoDinero> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/movimientos/{id}")
	public ResponseEntity<Optional<MovimientoDinero>> findOne(@PathVariable Integer id){
		
		Optional<MovimientoDinero> movimiento = service.findOne(id);
		if(movimiento != null) {
			return new ResponseEntity<>(movimiento, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/movimientos")
	public ResponseEntity<MovimientoDinero> saveOne(@RequestBody MovimientoDinero mov){
		
		try {
			MovimientoDinero movimiento = service.saveOne(mov);
			if(movimiento.getIdMovimiento() > 0){
				return new ResponseEntity<>(movimiento, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/movimientos/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody MovimientoDinero mov, @PathVariable Integer id){
		
		try {
			if(mov.getIdMovimiento() == id) {
				Integer filas = service.updateOne(mov, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@DeleteMapping("/api/movimientos/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
