package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.EstadoPlato;
import com.xxi.api.services.EstadoPlatoService;

@RestController
public class EstadoPlatoController {

	@Autowired
	EstadoPlatoService service;
	
	@GetMapping("/api/estadosplato")
	public ResponseEntity<List<EstadoPlato>> getAll(){
		
		List<EstadoPlato> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/estadosplato/{id}")
	public ResponseEntity<Optional<EstadoPlato>> findOne(@PathVariable Integer id){
		
		Optional<EstadoPlato> plato = service.findOne(id);
		if(plato != null) {
			return new ResponseEntity<>(plato, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/estadosplato")
	public ResponseEntity<EstadoPlato> saveOne(@RequestBody EstadoPlato est){
		
		try {
			EstadoPlato ep = service.saveOne(est);
			if(ep.getIdEstadoPlato() > 0){
				return new ResponseEntity<>(ep, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/estadosplato/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody EstadoPlato est, @PathVariable Integer id){
		
		try {
			if(est.getIdEstadoPlato() == id) {
				Integer filas = service.updateOne(est, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/estadosplato/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
