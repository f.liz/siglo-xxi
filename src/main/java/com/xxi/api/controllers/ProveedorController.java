package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Proveedor;
import com.xxi.api.services.ProveedorService;


@RestController
public class ProveedorController {

	@Autowired
	ProveedorService service;
	
	@GetMapping("/api/proveedores")
	public ResponseEntity<List<Proveedor>> getAll(){
		
		List<Proveedor> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/proveedores/{id}")
	public ResponseEntity<Optional<Proveedor>> findOne(@PathVariable Integer id){
		
		Optional<Proveedor> proveedor = service.findOne(id);
		if(proveedor != null) {
			return new ResponseEntity<>(proveedor, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/proveedores")
	public ResponseEntity<Proveedor> saveOne(@RequestBody Proveedor pro){
		
		try {
			Proveedor proveedor = service.saveOne(pro);
			if(proveedor.getIdProveedor() > 0){
				return new ResponseEntity<>(proveedor, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/proveedores/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Proveedor pro, @PathVariable Integer id){
		
		try {
			if(pro.getIdProveedor() == id) {
				Integer filas = service.updateOne(pro, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/proveedores/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
