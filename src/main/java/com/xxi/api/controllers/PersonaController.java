package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.Persona;
import com.xxi.api.services.PersonaService;


@RestController
public class PersonaController {

	
	@Autowired
	PersonaService service;
	
	@GetMapping("/api/personas")
	public ResponseEntity<List<Persona>> getAll(){
		
		List<Persona> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/personas/{id}")
	public ResponseEntity<Optional<Persona>> findOne(@PathVariable Integer id){
		
		Optional<Persona> personas = service.findOne(id);
		if(personas != null) {
			return new ResponseEntity<>(personas, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/personas")
	public ResponseEntity<Persona> saveOne(@RequestBody Persona pe){
		try {
			Persona persona = service.saveOne(pe);
			if(persona.getIdPerson() > 0){
				return new ResponseEntity<>(persona, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/personas/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody Persona pe, @PathVariable Integer id){
		
		try {
			if(pe.getIdPerson() == id) {
				Integer filas = service.updateOne(pe, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/personas/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
