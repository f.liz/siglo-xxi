package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.UnidadMedida;
import com.xxi.api.services.UnidadMedidaService;

@RestController
public class UnidadMedidaController {
	@Autowired
	UnidadMedidaService service;
	
	@GetMapping("/api/unidades")
	public ResponseEntity<List<UnidadMedida>> getAll(){
		
		List<UnidadMedida> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/unidades/{id}")
	public ResponseEntity<Optional<UnidadMedida>> findOne(@PathVariable Integer id){
		
		Optional<UnidadMedida> unidad = service.findOne(id);
		if(unidad != null) {
			return new ResponseEntity<>(unidad, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/unidades")
	public ResponseEntity<UnidadMedida> saveOne(@RequestBody UnidadMedida uni){
		
		try {
			UnidadMedida unidad = service.saveOne(uni);
			if(unidad.getIdUnidadMedida() > 0){
				return new ResponseEntity<>(unidad, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/unidades/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody UnidadMedida uni, @PathVariable Integer id){
		
		try {
			if(uni.getIdUnidadMedida() == id) {
				Integer filas = service.updateOne(uni, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/unidades/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
