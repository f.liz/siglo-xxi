package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.TipoOrden;
import com.xxi.api.services.TipoOrdenService;


@RestController
public class TipoOrdenController {

	@Autowired
	TipoOrdenService service;
	
	@GetMapping("/api/tiposorden")
	public ResponseEntity<List<TipoOrden>> getAll(){
		
		List<TipoOrden> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/tiposorden/{id}")
	public ResponseEntity<Optional<TipoOrden>> findOne(@PathVariable Integer id){
		
		Optional<TipoOrden> tipo = service.findOne(id);
		if(tipo != null) {
			return new ResponseEntity<>(tipo, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/tiposorden")
	public ResponseEntity<TipoOrden> saveOne(@RequestBody TipoOrden tip){
		
		try {
			TipoOrden tipo = service.saveOne(tip);
			if(tipo.getIdTipo() > 0){
				return new ResponseEntity<>(tipo, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/tiposorden/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody TipoOrden tip, @PathVariable Integer id){
		
		try {
			if(tip.getIdTipo() == id) {
				Integer filas = service.updateOne(tip, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/tiposorden/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
