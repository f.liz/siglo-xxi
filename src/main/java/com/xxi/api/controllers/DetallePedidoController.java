package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.DetallePedido;
import com.xxi.api.services.DetallePedidoService;



@RestController
public class DetallePedidoController {

	
	@Autowired
	DetallePedidoService service;
	
	@GetMapping("/api/detallepedidos")
	public ResponseEntity<List<DetallePedido>> getAll(){
		
		List<DetallePedido> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/detallepedidos/{id}")
	public ResponseEntity<Optional<DetallePedido>> findOne(@PathVariable Integer id){
		
		Optional<DetallePedido> detallepedido = service.findOne(id);
		if(detallepedido != null) {
			return new ResponseEntity<>(detallepedido, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/detallepedidos")
	public ResponseEntity<DetallePedido> saveOne(@RequestBody DetallePedido dp){
		try {
			DetallePedido detallepedido = service.saveOne(dp);
			if(detallepedido.getIdDetPed() > 0){
				return new ResponseEntity<>(detallepedido, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@PutMapping("/api/detallepedidos/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody DetallePedido dp, @PathVariable Integer id){
		
		try {
			if(dp.getIdDetPed() == id) {
				Integer filas = service.updateOne(dp, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
	
	@DeleteMapping("/api/detallepedidos/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
