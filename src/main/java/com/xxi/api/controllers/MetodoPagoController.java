package com.xxi.api.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xxi.api.entities.MetodoPago;
import com.xxi.api.services.MetodoPagoService;


@RestController
public class MetodoPagoController {

	@Autowired
	MetodoPagoService service;
	
	@GetMapping("/api/metodospago")
	public ResponseEntity<List<MetodoPago>> getAll(){
		
		List<MetodoPago> lista = service.getAll();
		if(lista.size() > 0) {
			return new ResponseEntity<>(lista, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/api/metodospago/{id}")
	public ResponseEntity<Optional<MetodoPago>> findOne(@PathVariable Integer id){
		
		Optional<MetodoPago> metodopago = service.findOne(id);
		if(metodopago != null) {
			return new ResponseEntity<>(metodopago, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/api/metodospago")
	public ResponseEntity<MetodoPago> saveOne(@RequestBody MetodoPago met){
		
		try {
			MetodoPago metodopago = service.saveOne(met);
			if(metodopago.getIdMetodoPago() > 0){
				return new ResponseEntity<>(metodopago, HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/api/metodospago/{id}")
	public ResponseEntity<Integer> updateOne(@RequestBody MetodoPago met, @PathVariable Integer id){
		
		try {
			if(met.getIdMetodoPago() == id) {
				Integer filas = service.updateOne(met, id);
				if(filas > 0) {
					return new ResponseEntity<>(filas, HttpStatus.OK);
				}else {
					return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
				}
			}else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/api/metodospago/{id}")
	public ResponseEntity<Integer> deleteOne(@PathVariable Integer id){
		
		try {
			Integer filas = service.deleteOne(id);
			if(filas > 0) {
				return new ResponseEntity<>(filas, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
